import React from 'react'
import { storiesOf, action } from '@kadira/storybook'
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider'
import PDFViewer from '../pdf-viewer/pdf-viewer.component.jsx'

export const props = {
  popularized: {
    title: 'Title',
    description: 'This is a video',
    url: 'http://www.pdf995.com/samples/pdf.pdf',
    createdAt: 'Jan. 15, 2016'
  }
}

storiesOf('PDF Viewer', module)
  .add('component', () => (
    <MuiThemeProvider>
      <div style={{width: 500, height: 300}}>
        <PDFViewer
          popularized={{
            title: 'Title',
            description: 'This is a video',
            url: 'http://www.pdf995.com/samples/pdf.pdf',
            createdAt: 'Jan. 15, 2016'
          }}
        />
      </div>
    </MuiThemeProvider>
  ))
