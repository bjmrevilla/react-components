import React from 'react'
import { storiesOf, action } from '@kadira/storybook'
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider'
import ManualViewer from '../manual-viewer.component.jsx';
import ManualChapterViewer
  from '../manual-chapter-viewer/manual-chapter-viewer.component.jsx';
import ManualDetailsViewer
  from '../manual-details-viewer/manual-details-viewer.component.jsx';
import {
  manual,
  props,
  chapterViewProps
} from './fixtures';

require('./pdf-viewer.js');
require('./video-viewer.js');

storiesOf('Manual Viewer', module)
  .add('with chapter viewer', () => (
    <MuiThemeProvider>
      <ManualViewer menus={props.menus} manual={manual}>
        <ManualChapterViewer
          {...chapterViewProps}
          />
      </ManualViewer>
    </MuiThemeProvider>
  ))

storiesOf('Manual Viewer', module)
  .add('with details viewer', () => (
    <MuiThemeProvider>
      <ManualViewer menus={props.menus} manual={manual}>
        <ManualDetailsViewer
          manual = {{
            title: 'Manual',
            description: 'Manual description, etc.',
            creator: 'creator',
            version: '1',
            publisher: 'publisher',
            publishedAt: 'Jan 16, 2016'
          }}
          />
      </ManualViewer>
    </MuiThemeProvider>
  ))