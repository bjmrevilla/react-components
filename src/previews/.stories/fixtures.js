import { storiesOf, action } from '@kadira/storybook'

export const manual = {
  draftDescription: 'Material Design for Dummies'
};

export const props = {
  onManualDetails: action('onManual'),
  menus: [
    {key: 1, label: 'label 1', link: '#', onClick: action('click')},
    {key: 2, label: 'label 2', link: '#', onClick: action('click')},
    {key: 3, label: 'label 3', link: '#', onClick: action('click')},
    {key: 4, label: 'label 4', link: '#', onClick: action('click')},
    {key: 5, label: 'label 5', link: '#', onClick: action('click')},
  ]
};

export const chapterViewProps = {
  chapter: {
    label: 'label',
    title: 'title',
    body: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Cras feugiat lectus non lobortis facilisis. Duis eu malesuada nunc. Duis vitae lorem id nibh sagittis commodo sed quis dolor. Morbi viverra neque in ultrices tincidunt. Sed fringilla metus mollis quam rhoncus, et bibendum risus consectetur. Vestibulum placerat rhoncus pharetra. Donec fringilla est ac libero consectetur congue eget sed nulla. Donec in nibh nisl. Nullam tincidunt, dui vel blandit viverra, ex arcu euismod neque, at suscipit sem orci in felis.'
  }
};