import React from 'react'
import { storiesOf, action } from '@kadira/storybook'
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider'
import VideoViewer from '../video-viewer/video-viewer.component.jsx'

storiesOf('Video Viewer', module)
  .add('component', () => (
    <MuiThemeProvider>
      <div style={{width: 500}}>
        <VideoViewer
          video={{
            title: 'Title',
            description: 'This is a video',
            url: 'http://kalahi-web.herokuapp.com/video/575a53a0c4d203110033f9b0',
            createdAt: 'Jan. 15, 2016'
          }}
        />
      </div>
    </MuiThemeProvider>
  ))
