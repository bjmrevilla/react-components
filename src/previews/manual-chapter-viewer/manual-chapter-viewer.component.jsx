'use strict';

import React from 'react';
import styles from './manual-chapter-viewer.scss';
import { compose, flattenProp } from 'recompose';
import RaisedButton from 'material-ui/RaisedButton';

export default class ManualChapterViewer extends React.Component {
  constructor(props) {
    super(props);
  }

  static propTypes = {
    chapter: React.PropTypes.object
  }

  render = () => {
    const {
      chapter: { label, title, body }
    } = this.props;

    return (
      <div className={styles.root}>
        <h1 className={styles.title}>
          <small className={styles.label}>{label}</small> {title}
        </h1>
        <div>{body}</div>
        <br />
        <div>
          <div className={styles.warningMsg}>
            <span className={styles.warning}>Warning!</span> Next Chapter is encrypted answer questions to proceed
          </div>
          <RaisedButton className={styles.answerQuestion} label='Answer questions' primary />
        </div>
      </div>
    );
  }
}