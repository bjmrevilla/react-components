'use strict';

import React from 'react';
import styles from './manual-details-viewer.scss';
import { compose, flattenProp } from 'recompose';

export default class ManualDetailsViewer extends React.Component {
  constructor(props) {
    super(props);
  }

  static propTypes = {
    manual: React.PropTypes.object
  }

  render = () => {
    const {
      manual: {
        title,
        description,
        creator,
        version,
        publisher,
        publishedAt
      }
    } = this.props;

    return (
      <div className={styles.root}>
        <div className={styles.meta}>
          <div>
            <h1 className={styles.top}>
              {title} <small>v.{version}</small>
            </h1>
            <p className={styles.description}>{description}</p>
            <p>by {creator}</p>
          </div>
          <div className={styles.publisher}>
            <p className={styles.top}>{publisher}</p>
            <p className={styles.publishedAt}>{publishedAt}</p>
          </div>
        </div>
        <div>Tag List</div>
        <div>Tag List</div>
        <div>Tag List</div>
        <div>Tag List</div>
      </div>
    );
  }
}