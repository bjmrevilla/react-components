'use strict';

import React from 'react';
import styles from './manual-navigation.scss';
import { compose, flattenProp } from 'recompose';
import {List, ListItem} from 'material-ui/List';
import FlatButton from 'material-ui/FlatButton';
import Drawer from 'material-ui/Drawer';

export default class ManualNavigation extends React.Component {
  constructor(props) {
    super(props);
  }

  static propTypes = {
    menus: React.PropTypes.arrayOf(React.PropTypes.shape({
      key: React.PropTypes.any.isRequired,
      label: React.PropTypes.string.isRequired,
      link: React.PropTypes.string,
      disable: React.PropTypes.bool,
      onTouchTap: React.PropTypes.func
    })),
    showManualDetails: React.PropTypes.bool,
    open: React.PropTypes.bool
  }

  static defaultProps = {
    showManualDetails: true,
    open: false
  }

  render = () => {
    const { menus, showManualDetails, onManualDetails } = this.props;

    return (
      <Drawer
        className={styles.root}
        open={this.props.open}
        docked={false}
        onRequestChange={this.props.onCloseDrawer}>
        <List className={styles.links}>
          {menus.map(({key, label, link, disable, onClick}) => (
            <div>
              <ListItem
                key={key}
                href={disable ? '' : link}
                onClick={disable ? '' : e => onClick(key) }
                primaryText={label} />
            </div>
          )) }
        </List>
        {showManualDetails
          ? <div className={styles.manualDetails}>
            <FlatButton
              onClick={onManualDetails}
              style={{width: '100%'}}>
              Manual Details
            </FlatButton>
          </div>
          : ''
        }
      </Drawer>
    );
  }
}