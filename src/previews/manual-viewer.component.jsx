'use strict';

import React from 'react';
import ManualNavigation
  from './manual-navigation/manual-navigation.component.jsx';
import styles from './manual-viewer.scss';
import AppBar from 'material-ui/AppBar';

export default class ManualViewer extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      isNavigationOpen: false
    };
  }

  static propTypes = {
    manual: React.PropTypes.object,
    manualUse: React.PropTypes.object
  }

  handleClickBurger = () => {
    this.setState({
      isNavigationOpen: !this.state.isNavigationOpen
    });
  }

  handleCloseDrawer = () => {
    this.setState({
      isNavigationOpen: false
    });
  }

  render = () => {
    return (
      <div className={styles.root}>
        <AppBar
          title={this.props.manual.draftDescription}
          onLeftIconButtonTouchTap={this.handleClickBurger} />
        <ManualNavigation
          menus={this.props.menus}
          onCloseDrawer={this.handleCloseDrawer}
          open={this.state.isNavigationOpen} />
        <div className={styles.child}>
          {this.props.children}
        </div>
      </div>
    );
  }
}