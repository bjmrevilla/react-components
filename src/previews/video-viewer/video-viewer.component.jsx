'use strict';

import React from 'react';
import { compose, flattenProp } from 'recompose';
import styles from './video-viewer.scss';

export default compose(
  flattenProp('video')
)(({ title, description, url, createdAt }) => (
  <div className={styles.root}>
    <video
      controls
      className={styles.video}
      src={url}
    >
      Your browser does not support the <code>video</code> element.
    </video>
    <div className={styles.meta}>
      <div>
        <h1>{title}</h1>
        <p className={styles.description}>{description}</p>
      </div>
      <div className={styles.date}>
        <p>{createdAt}</p>
      </div>
    </div>
  </div>
));
