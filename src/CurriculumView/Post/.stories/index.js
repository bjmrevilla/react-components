import React from 'react'
import { storiesOf, action } from '@kadira/storybook'
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider'
import Post from '../index.js'

const props = {
  post: {
    creator: {
      username: 'Secretmapper',
      email: 'Secretmapper16@gmail.com',
      createdAt: 'Janualy 16, 2016'
    },
    title: 'Post title',
    body: `Lorem ipsum dlor sit amet, consectetur adipiscing elit.
    Donec mattis pretium massa. Aliquam erat volutpat. Nulla facilisi.
    Donec vulputate interdum sollicitudin. Nunc lacinia auctor quam sed pellentesque.
    Aliquam dui mauris, mattis quis lacus id, pellentesque lobortis odio.`
  },
  onLike: action('like')
}
storiesOf('curriculumView.post', module)
  .add('component', () => (
    <MuiThemeProvider>
      <Post {...props} />
    </MuiThemeProvider>
  ))
  .add('canLike canComment', () => (
    <MuiThemeProvider>
      <Post {...props} canLike canComment />
    </MuiThemeProvider>
  ))
