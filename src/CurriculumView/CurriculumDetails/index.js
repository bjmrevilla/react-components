import React from 'react'
import Paper from 'material-ui/Paper'
import TextField from 'material-ui/TextField'
import FlatButton from 'material-ui/FlatButton'
import RaisedButton from 'material-ui/RaisedButton'

import TagList from '../../ListView/TagList'

import styles from './index.scss'

export default class Post extends React.Component {
  constructor () {
    super()
    this.state = { content: '' }
  }

  static defaultProps = {
    curriculum: {
      title: '',
      description: ''
    },
    canEnroll: false,
    readOnly: false
  }

  render () {
    const {
      curriculum: { title, description },
      canEnroll,
      readOnly,
      image,
      onChangeTitle,
      onChangeDescription,
      onCancel,
      onSave,
      onUploadCover,
      onQuestions
    } = this.props

    return (
      <Paper className={styles.root}>
        <div className={styles.cover}>
          <div className={styles.imageHolder}>
            <img
              src={image ? image : ''}
              className={styles.image}
            />
          </div>
          {readOnly
            ? ''
            : <FlatButton label='Upload Cover' onClick={onUploadCover} />
          }
        </div>
        <div>
          {readOnly
            ? ''
            : <div className={styles.auxButtons}>
              <FlatButton label='Cancel' onClick={onCancel} secondary />
              <RaisedButton label='Save' onClick={onSave} primary />
            </div>
          }
          <TextField
            id='title'
            floatingLabelText='title'
            value={title}
            onChange={e => onChangeTitle(e.target.value)}
            disabled={readOnly}
            fullWidth
          />
          <TextField
            id='description'
            floatingLabelText='description'
            value={description}
            onChange={e => onChangeDescription(e.target.value)}
            disabled={readOnly}
            fullWidth
          />
          {canEnroll
            ? <RaisedButton
              label='Questions/Enroll'
              className={styles.enroll}
              onClick={onQuestions}
              primary
            />
          : ''}
        </div>
      </Paper>
    )
  }
}
