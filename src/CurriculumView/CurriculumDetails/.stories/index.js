import React from 'react'
import { storiesOf, action } from '@kadira/storybook'
import { compose, withState } from 'recompose'
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider'
import CurriculumDetails from '../index.js'
import { props as tagList } from '../../../ListView/TagList/.stories'

const Wrapped = compose(
  withState('title', 'onChangeTitle', ''),
  withState('description', 'onChangeDescription', '')
)(({ title, description, ...props }) => (
  <MuiThemeProvider>
    <CurriculumDetails
      image='http://kalahi-web.herokuapp.com/cdn/577e87a0ea22591100c94601.png'
      curriculum={{ title, description }}
      onCancel={action('onCancel')}
      onSave={action('onSave')}
      onUploadCover={action('onUploadCover')}
      onQuestions={action('onQuestions')}
      {...props}
    />
  </MuiThemeProvider>
))

storiesOf('curriculumView.curriculumDetails', module)
  .add('component', () => (
    <Wrapped />
  ))
  .add('canEnroll', () => (
    <Wrapped canEnroll={true} />
  ))
  .add('readOnly', () => (
    <Wrapped readOnly={true} />
  ))
