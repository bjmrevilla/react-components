import React from 'react'
import TinyMCE from 'react-tinymce-input'
import TextField from 'material-ui/TextField'
import FlatButton from 'material-ui/FlatButton'

import styles from './index.scss'

export default class Post extends React.Component {
  constructor () {
    super()
    this.state = { content: '' }
  }

  onChangeTitle = (e) => this.setState({ title: e.target.value })
  onChangeContent = (content) => this.setState({ content })

  render () {
    const { onChangeContent, onChangeTitle } = this
    const { onPost } = this.props
    const { title, content } = this.state

    return (
      <div>
        <FlatButton
          onClick={e => onPost({title, content})}
          label='Post'
          primary
          className={styles.submit}
        />
        <TextField
          type='text'
          placeholder='Title'
          value={title}
          onChange={onChangeTitle}
          className={styles.title}
        />
        <TinyMCE
          ref='tinyMCE'
          tinymceConfig={{
            plugins: 'autolink link image table lists paste preview',
            toolbar: false,
            menubar: false,
            height: '200',
            content_style: 'img { height: auto; max-width: 100%; }',
          }}
          value={content}
          onChange={onChangeContent}
        />
      </div>
    )
  }
}
