import React from 'react'
import { storiesOf, action } from '@kadira/storybook'
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider'
import PostEditor from '../index.js'

const props = {
}
storiesOf('curriculumView.postEditor', module)
  .add('component', () => (
    <MuiThemeProvider>
      <PostEditor onPost={action('post')} {...props} />
    </MuiThemeProvider>
  ))
