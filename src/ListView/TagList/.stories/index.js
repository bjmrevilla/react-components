import React from 'react'
import { storiesOf, action } from '@kadira/storybook'
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider'
import TagList from '../index'

const data = {
  title: 'Sample Book',
  subtitle: 'Book Subtitle',
  image: 'http://kalahi-web.herokuapp.com/cdn/577e87a0ea22591100c94601.png'
}
const title = 'title'
const description = 'description for tag list'
const tip = 'this is a tip'
const label = 'label:'
const tags = [
  { key: 'Chip' },
  { key: 'Chap' },
  { key: 'Chop' }
]

export const props = { title, description, tip, label, tags }

storiesOf('listView.tagList', module)
  .add('component', () => (
    <MuiThemeProvider>
      <TagList
        {...props}
      />
    </MuiThemeProvider>
  ))
  .add('disableAddNew = false', () => (
    <MuiThemeProvider>
      <TagList
        {...props}
        disableAddNew={false}
        add={action('add')}
      />
    </MuiThemeProvider>
  ))
  .add('readOnly', () => (
    <MuiThemeProvider>
      <TagList
        {...props}
        readOnly
      />
    </MuiThemeProvider>
  ))
  .add('!showTip !showLabel', () => (
    <MuiThemeProvider>
      <TagList
        {...props}
        showDescription={false}
        showTip={false}
      />
    </MuiThemeProvider>
  ))
