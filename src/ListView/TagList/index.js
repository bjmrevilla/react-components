import React, { PropTypes } from 'react'
import Chip from 'material-ui/Chip'
import Paper from 'material-ui/Paper'
import IconButton from 'material-ui/IconButton'
import RaisedButton from 'material-ui/RaisedButton'
import ContentAdd from 'material-ui/svg-icons/content/add'

import styles from './index.scss'

export default class TagsList extends React.Component {
  constructor (props) {
    super(props)
  }

  static propTypes = {
    tags: PropTypes.arrayOf(PropTypes.object),
    title: PropTypes.string,
    description: PropTypes.string,
    tip: PropTypes.string,
    label: PropTypes.string,
    showTitle: PropTypes.bool,
    showDescription: PropTypes.bool,
    showTip: PropTypes.bool,
    disableAddNew: PropTypes.bool,
    disableRemove: PropTypes.bool,
    readOnly: PropTypes.bool,
    add: PropTypes.func,
    hideFloatingAddButton: PropTypes.bool,
    hideAddButton: PropTypes.bool
  }

  static defaultProps = {
    showTitle: true,
    showDescription: true,
    showTip: true,
    disableAddNew: true,
    disableRemove: false,
    hideFloatingAddButton: false,
    hideAddButton: false,
    readOnly: false
  }

  render () {
    const { title, description, tip, label, tags, add } = this.props
    const { showTitle, showDescription, showTip } = this.props
    const { hideAddButton, hideFloatingAddButton } = this.props
    const { readOnly } = this.props

    let { disableAddNew, disableRemove } = this.props

    if (readOnly) {
      disableAddNew = true
      disableRemove = true
    }

    return (
      <Paper className={styles.root}>
        {showTitle
          ? <h1 className={styles.title}>{title}</h1>
          : ''
        }
        {showDescription
          ? <div className={styles.desc}>{description}</div>
          : ''
        }
        {showTip
          ? <div className={styles.tip}>{tip}</div>
          : ''
        }
        <div className={styles.chips}>
          <div className={styles.label}>{label}</div>
          {tags.map(({key}) => <Chip key={key} className={styles.chip}>{key}</Chip>)}
          {disableAddNew || hideAddButton
            ? ''
            : <IconButton onClick={add}><ContentAdd /></IconButton>
          }
        </div>
        {disableAddNew || hideFloatingAddButton
          ? ''
          : <RaisedButton
            className={styles.add}
            label='Add'
            primary={true}
            onClick={add}
          />
        }
      </Paper>
    )
  }
}
