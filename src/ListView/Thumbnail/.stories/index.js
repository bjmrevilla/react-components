import React from 'react'
import { storiesOf, action } from '@kadira/storybook'
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider'
import Thumbnail from '../index'
import props from './fixtures'

storiesOf('listView.thumbnail', module)
  .add('!hideImage && actionsPosition === bottom', () => (
    <MuiThemeProvider>
      <div style={{ width: 250 }}>
        <Thumbnail
          {...props}
          hideImage={false}
          actionsPosition={'bottom'}
        />
      </div>
    </MuiThemeProvider>
  ))
  .add('hideImage && emphasize actionsPosition === top', () => (
    <MuiThemeProvider>
      <div style={{ width: 250 }}>
        <Thumbnail
          {...props}
          actionsPosition={'top'}
          emphasize
          hideImage
        />
      </div>
    </MuiThemeProvider>
  ))
  .add('imageOnly', () => (
    <MuiThemeProvider>
      <div style={{ width: 250 }}>
        <Thumbnail
          {...props}
          imageOnly={true}
        />
      </div>
    </MuiThemeProvider>
  ))

