import { action } from '@kadira/storybook'

const data = {
  title: 'Sample Book',
  subtitle: 'Book Subtitle',
  image: 'http://kalahi-web.herokuapp.com/cdn/577e87a0ea22591100c94601.png'
}
const title = { key: 'title' }
const subtitle = { key: 'subtitle' }
const image = { key: 'image' }
const actions = [
  { label: 'open', fn: action('open') },
  { label: 'close', fn: action('close') }
]

export default { data, title, subtitle, image, actions }
