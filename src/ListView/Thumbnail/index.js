import React, { PropTypes } from 'react';
import cs from 'classnames'
import {Card, CardActions, CardHeader, CardMedia, CardTitle, CardText} from 'material-ui/Card'
import IconButton from 'material-ui/IconButton'
import ActionReorder from 'material-ui/svg-icons/action/reorder'
import Popover from 'material-ui/Popover'
import Menu from 'material-ui/Menu'
import MenuItem from 'material-ui/MenuItem'

import MaterialInfo from './MaterialInfo'

import styles from './thumbnail.scss';

const ObjectKey = PropTypes.shape({
  label: PropTypes.string,
  key: PropTypes.string.isRequired,
  format: PropTypes.func
})

export default class Thumbnail extends React.Component {
  constructor(props) {
    super(props)

    this.state = {
      open: false
    }
  }

  static propTypes = {
    data: PropTypes.object,
    title: ObjectKey,
    subtitle: ObjectKey,
    image: ObjectKey,
    defaultImage: PropTypes.string,
    hideImage: PropTypes.bool,
    emphasize: PropTypes.bool,
    imageOnly: PropTypes.bool,
    actions: PropTypes.arrayOf(PropTypes.shape({
                label: PropTypes.string,
                fn: PropTypes.func
              })),
    actionsPosition: PropTypes.string
  }

  static defaultProps = {
    actionsPosition: 'top',
    hideImage: false,
    emphasize: false,
    imageOnly: false
  }

  handleTouchTap = (event) => {
    // This prevents ghost click.
    event.preventDefault();
    console.log('a')

    this.setState({
      open: true,
      anchorEl: event.currentTarget,
    });
  };

  handleRequestClose = () => {
    this.setState({
      open: false,
    });
  };

  render () {
    const { data, title, subtitle, image, actions, actionPosition } = this.props
    const { actionsPosition, emphasize, hideImage, imageOnly } = this.props
    const expand = hideImage || imageOnly

    const p = (item) => {
      if (item) {
        const { label, key, format } = item
        return (format) ? format(data[key]) : data[key]
      } else {
        return ''
      }
    }

    return (
      <Card className={styles.root}>
        <CardMedia>
        {hideImage
          ? <div></div>
          : <div className={cs(styles.imageHolder, {[styles.imageOnly]: imageOnly})}>
            <img
              src={hideImage ? '' : p(image)}
              className={styles.image}
            />
          </div>
        }
        </CardMedia>
        {!imageOnly
          ? <CardTitle>
            <div className={cs({[styles.emphasize]: emphasize})}>
              <p className={cs(styles.title, styles.removeMargin)}>
                {p(title)}
              </p>
              <p className={cs(styles.subtitle, styles.removeMargin)}>
                {p(subtitle)}
              </p>
            </div>
          </CardTitle>
          : ''
        }
        <IconButton
          tooltip='actions'
          className={cs({
            [styles.actionsPositionTop]: actionsPosition === 'top',
            [styles.actionsPositionBottom]: actionsPosition === 'bottom'
          })}
          onClick={this.handleTouchTap}
        >
          <ActionReorder />
        </IconButton>
        <Popover
          open={this.state.open}
          anchorEl={this.state.anchorEl}
          anchorOrigin={{horizontal: 'left', vertical: 'bottom'}}
          targetOrigin={{horizontal: 'left', vertical: 'top'}}
          onRequestClose={this.handleRequestClose}
        >
          <Menu>
            {actions.map(({label, fn}) => (
              <MenuItem key={label} primaryText={label} onClick={fn} />
            ))}
          </Menu>
        </Popover>
      </Card>
    )
  }
}
