import React, { PropTypes } from 'react'
import TextField from 'material-ui/TextField'
import SelectField from 'material-ui/SelectField'
import MenuItem from 'material-ui/MenuItem'
import FloatingActionButton from 'material-ui/FloatingActionButton';
import Left from 'material-ui/svg-icons/navigation/chevron-left'
import Right from 'material-ui/svg-icons/navigation/chevron-right'
import styles from './index.scss'

export default class CarouselView extends React.Component {

  constructor(props) {
    super(props);

    this.state = {value: 1};
  }

  onChangeSort = (event, index, value) => {
    this.setState({ sort: value })
  }

  onLeft = () => {
    this.refs.row.scrollLeft -= 100
  }

  onRight = () => {
    this.refs.row.scrollLeft += 100
  }

  onUpdateSearch = (e) => {
    e.persist()
    const { onUpdateSearch } = this.props
    onUpdateSearch(e.target.value)
  }

  render () {
    const { onUpdateSearch } = this
    const { children, search } = this.props
    const { sort } = this.state

    return (
      <div className={styles.root}>
        <TextField
          id='search'
          value={search}
          placeholder='Search'
          onChange={onUpdateSearch}
        />
        <SelectField
          className={styles.select}
          value={sort}
          onChange={this.onChangeSort}
          hintText='Sort by'
        >
          <MenuItem value={1} primaryText='Name' />
          <MenuItem value={2} primaryText='Date' />
        </SelectField>
        <div className={styles.rowContainer}>
          <FloatingActionButton onClick={this.onLeft} className={styles.rowLeft}>
            <Left />
          </FloatingActionButton>
          <FloatingActionButton onClick={this.onRight} className={styles.rowRight}>
            <Right />
          </FloatingActionButton>
          <div ref='row' className={styles.row}>
            {children}
          </div>
        </div>
      </div>
    )
  }
}
