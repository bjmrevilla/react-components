import React from 'react'
import { storiesOf, action } from '@kadira/storybook'
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider'
import CarouselView from '../index'
import Thumb from '../../Thumbnail'
import props from '../../Thumbnail/.stories/fixtures'

const Thumbnail = () => (
  <div style={styles.thumbnail}>
    <Thumb {...props} />
  </div>
)

storiesOf('listView.carouselView', module)
  .add('component', () => (
    <MuiThemeProvider>
      <CarouselView search='' onUpdateSearch={action('onUpdateSearch')}>
        <Thumbnail key={1} {...props} />
        <Thumbnail key={2} {...props} />
        <Thumbnail key={3} {...props} />
        <Thumbnail key={4} {...props} />
        <Thumbnail key={5} {...props} />
      </CarouselView>
    </MuiThemeProvider>
  ))

const styles = {
  thumbnail: {
    display:'inline-block',
    padding: 10,
    width: 200,
    verticalAlign: 'middle'
  }
}
