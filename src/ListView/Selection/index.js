import React, { PropTypes } from 'react';
import cs from 'classnames'
import R from 'ramda'
import styles from './index.scss'
import Checkbox from 'material-ui/Checkbox'
import TextField from 'material-ui/TextField'
import FlatButton from 'material-ui/FlatButton'
import Paper from 'material-ui/Paper'

export default class Selection extends React.Component {
  constructor(props) {
    super(props)

    this.state = {
      addTag: ''
    }
  }

  static propTypes = {
    title: PropTypes.string,
    sourceItems: PropTypes.arrayOf(PropTypes.object),
    selectedItems: PropTypes.arrayOf(PropTypes.object),
    disableAddNow: PropTypes.bool,
    onSelectedItemsChanged: PropTypes.func,
    add: PropTypes.func,
    search: PropTypes.func
  }

  static defaultProps = {
    sourceItems: [],
    selectedItems: []
  }

  inSelected (key) {
    const { selectedItems } = this.props
    return (R.filter(R.propEq('key', key))(selectedItems)).length > 0
  }

  changeTag = (e) => {
    this.setState({ addTag: e.target.value })
  }

  render () {
    const { title, sourceItems, selectedItems, onSelectedItemsChanged } = this.props
    const { add, search } = this.props

    return (
      <Paper className={styles.root}>
        <h1 className={styles.title}>{title}</h1>
        <TextField onChange={e => search(e.target.value)} hintText='Search' />
        <div className={styles.checkboxes}>
        {sourceItems.map(({key}) => (
          <Checkbox
            key={key}
            label={key}
            checked={this.inSelected(key)}
            onCheck={(e, t) => { onSelectedItemsChanged(key, t) }}
          />
        ))}
        </div>
        <TextField
          value={this.state.addTag}
          onChange={this.changeTag}
          className={styles.textField}
          hintText='Add a Tag'
        />
        <FlatButton label='Add' primary onClick={e => add(this.state.addTag)} />
      </Paper>
    )
  }
}
