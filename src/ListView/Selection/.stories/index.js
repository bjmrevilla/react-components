import React from 'react'
import { storiesOf, action } from '@kadira/storybook'
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider'
import Selection from '../index'
import { compose, withState } from 'recompose'

const sourceItems = [
  { key: 'Option 1' },
  { key: 'Option 2' },
  { key: 'Option 3' },
  { key: 'Option 4' },
  { key: 'Option 5' },
  { key: 'Option 6' },
]
const props = { title : 'Title', sourceItems }

storiesOf('listView.selection', module)
  .add('component', () => {
    const selectedItems = [
      { key: 'Option 1' },
      { key: 'Option 5' },
    ]

    return (
      <MuiThemeProvider>
        <Selection
          onSelectedItemsChanged={action('onSelectedItemsChanged')}
          add={action('add')}
          selectedItems={selectedItems}
          search={action('search')}
          {...props}
        />
      </MuiThemeProvider>
    )
  })
