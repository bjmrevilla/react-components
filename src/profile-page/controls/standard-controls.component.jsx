'use strict';

import React from 'react';
import Loading from './loading.component.jsx';
import styles from './standard-controls.scss';
import RaisedButton from 'material-ui/RaisedButton';
import FlatButton from 'material-ui/FlatButton';

export default class StandardControls extends React.Component {
  constructor(props) {
    super(props);
  }

  static propTypes = {
    saveStatus: React.PropTypes.oneOf(['loaded', 'loading', 'failed']),
    save: React.PropTypes.func.isRequired,
    changePassword: React.PropTypes.func.isRequired,
    cancel: React.PropTypes.func.isRequired
  }

  static defaultProps = {
    saveStatus: 'loaded'
  }

  handleChangePassword = () => {
    //TODO: do something, maybe show modal?

    // this.props.changePassword();
  }

  getIsDisabled = () => {
    return (this.props.saveStatus === 'loading');
  }

  render() {
    return (
      <div className={styles.root}>
        <RaisedButton className={styles.changePasswordButton}
          label='Change Password'
          backgroundColor='#4CAF50'
          labelColor='white'
          onClick={this.handleChangePassword}
          disabled={this.getIsDisabled()} />
        <Loading saveStatus={this.props.saveStatus} />
        <FlatButton className={styles.cancelButton}
          label='Cancel'
          labelStyle={{color: '#D50000'}}
          onClick={this.props.cancel}
          disabled={this.getIsDisabled()} />
        <RaisedButton className={styles.saveButton}
          label='Save Changes'
          primary={true}
          onClick={this.props.save}
          disabled={this.getIsDisabled()} />
      </div>
    );
  }
}