'use strict';

import React from 'react';
import StandardControls from './standard-controls.component.jsx';
import DangerControls from './danger-controls.component.jsx';
import styles from './controls.scss';
import RaisedButton from 'material-ui/RaisedButton';
import FlatButton from 'material-ui/FlatButton';

export default class Controls extends React.Component {
  constructor(props) {
    super(props);
  }

  static propTypes = {
    saveStatus: React.PropTypes.oneOf(['loaded', 'loading', 'failed']),
    save: React.PropTypes.func.isRequired,
    delete: React.PropTypes.func.isRequired,
    changePassword: React.PropTypes.func.isRequired,
    cancel: React.PropTypes.func.isRequired,
    id: React.PropTypes.string.isRequired,
    readOnly: React.PropTypes.bool
  }

  static defaultProps = {
    saveStatus: 'loaded',
    readOnly: false
  }

  render() {
    return (this.props.readOnly)
      ? ''
      : (
        <div className={styles.root}>
          <StandardControls
            saveStatus={this.props.saveStatus}
            save={this.props.save}
            changePassword={this.props.changePassword}
            cancel={this.props.cancel} />
          <DangerControls
            saveStatus={this.props.saveStatus}
            delete={this.props.delete}
            id={this.props.id} />
        </div>
      );
  }
}