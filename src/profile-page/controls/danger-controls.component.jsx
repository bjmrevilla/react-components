'use strict';

import React from 'react';
import styles from './danger-controls.scss';
import RaisedButton from 'material-ui/RaisedButton';

export default class DangerControls extends React.Component {
  constructor(props) {
    super(props);
  }

  static propTypes = {
    saveStatus: React.PropTypes.oneOf(['loaded', 'loading', 'failed']),
    delete: React.PropTypes.func.isRequired,
    id: React.PropTypes.string.isRequired
  }

  static defaultProps = {
    saveStatus: 'loaded'
  }

  handleDelete = () => {
    // show some sort of confirmation dialog

    this.props.delete(this.props.id, (err) => {
      //should show loading and disable buttons
      //while deleting
    });
  }

  getIsDisabled = () => {
    return (this.props.saveStatus === 'loading');
  }

  render() {
    return (
      <div className={styles.root}>
        <div className={styles.warning}>
          <h4 className={styles.warningHeader}>
            Delete Account - Danger!
          </h4>
          <span className={styles.warningText}>
            Careful! Deleting your account cannot be undone. Please proceed
            with caution.
          </span>
        </div>
        <RaisedButton className={styles.deleteButton}
          label='Delete'
          backgroundColor='red'
          labelColor='white'
          onClick={this.handleDelete}
          disabled={this.getIsDisabled()} />
      </div>
    );
  }
}