'use strict';

import React from 'react';
import Fields from './fields.component.jsx';
import ProfilePicture from './profile-picture.component.jsx';
import styles from './personal-details.scss';

export default class PersonalDetails extends React.Component {
  constructor(props) {
    super(props);

    this.state = { changes: {} };
  }

  static propTypes = {
    saveStatus: React.PropTypes.oneOf(['loaded', 'loading', 'failed']),
    account: React.PropTypes.object.isRequired,
    readOnly: React.PropTypes.bool,
    onChange: React.PropTypes.func.isRequired,
    uploadProfilePic: React.PropTypes.func.isRequired
  }

  static defaultProps = {
    saveStatus: 'loaded',
    readOnly: false
  }

  componentDidMount() {
    this.handleUpdate();
  }

  componentDidUpdate() {
    this.handleUpdate();
  }

  handleUpdate = () => {
    // don't update if there are no changes to prevent infinite render loop
    if (Object.keys(this.state.changes).length !== 0) {
      this.props.onChange(this.state.changes);

      this.setState({ changes: {} });
    }
  }

  handleChange = (field, value) => {
    // append field-value pair instead of creating new object altogether
    var newChanges = this.state.changes;
    newChanges[field] = value;

    this.setState(newChanges);
  }

  getFields = () => {
    return (
      <Fields account={this.props.account}
        onChange={this.handleChange}
        disabled={this.getIsDisabled()} />
    );
  }
  
  getIsDisabled = () => {
    return (this.props.saveStatus === 'loading')
      || (this.props.readOnly);
  }

  render() {
    return (
      <div className={styles.root}>
        <div className={styles.profilePicture}>
          <ProfilePicture
            profilePictureSrc={this.props.account.pic}
            readOnly={this.props.readOnly}
            disabled={this.getIsDisabled()}
            uploadProfilePic={this.props.uploadProfilePic} />
        </div>
        <div className={styles.fields}>
          {this.getFields() }
        </div>
      </div>
    );
  }
}