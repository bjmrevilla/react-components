'use strict';

import React from 'react';
import FieldsRow from './fields-row.component.jsx';
import styles from './fields.scss';

export default class Fields extends React.Component {
  constructor(props) {
    super(props);
  }

  static propTypes = {
    account: React.PropTypes.object.isRequired,
    onChange: React.PropTypes.func.isRequired,
    disabled: React.PropTypes.bool
  }

  static defaultProps = {
    disabled: false
  }

  getFieldsRows = () => {
    //set this up because one way or the other, I'll have to hard code this
    var fieldArrangement =
      [['username', 'email'],
       ['firstName', 'middleName', 'lastName'],
       ['postalCode', 'street1', 'street2'],
       ['municipality', 'province', 'region'],
       ['country']];

    return fieldArrangement.map((rowArrangement, index) => {
      var rowData = {};

      for (var i in rowArrangement) {
        var key = rowArrangement[i];
        rowData[key] = this.props.account[key];
      }

      return (
        <FieldsRow
          key={index}
          fields={rowData}
          onChange={this.props.onChange}
          disabled={this.props.disabled} />
      );
    });   
  }

  render() {
    return (
      <div className={styles.root}>
        {this.getFieldsRows() }
      </div>
    );
  }
}