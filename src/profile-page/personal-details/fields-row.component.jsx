'use strict';

import React from 'react';
import Field from './field.component.jsx';
import styles from './fields-row.scss';

export default class FieldsRow extends React.Component {
  constructor(props) {
    super(props);
  }

  static propTypes = {
    fields: React.PropTypes.object.isRequired,
    onChange: React.PropTypes.func.isRequired,
    disabled: React.PropTypes.bool
  }

  static defaultProps = {
    disabled: false
  }

  getRowItems = () => {
    return Object.keys(this.props.fields).map((key) => {
      var value = this.props.fields[key];
      return (
        <Field
          key={key}
          field={key}
          value={value}
          onChange={this.props.onChange}
          disabled={this.props.disabled} />
      );
    });
  }

  render() {
    return (
      <div className={styles.root}>
        {this.getRowItems() }
      </div>
    );
  }
}