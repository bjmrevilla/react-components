'use strict';

import React from 'react';
import styles from './profile-picture.scss';
import Paper from 'material-ui/Paper';
import RaisedButton from 'material-ui/RaisedButton';

export default class ProfilePicture extends React.Component {
  constructor(props) {
    super(props);
  }

  static propTypes = {
    profilePictureSrc: React.PropTypes.string,
    readOnly: React.PropTypes.bool,
    disabled: React.PropTypes.bool,
    uploadProfilePic: React.PropTypes.func.isRequired
  }

  static defaultProps = {
    readOnly: false,
    disabled: false
  }

  getUploadButton = () => {
    return (this.props.readOnly)
      ? ''
      : (
          <RaisedButton
            onClick={this.props.uploadProfilePic}
            label='Upload Picture'
            primary={true}
            disabled={this.props.disabled} />
      );
  }

  render() {
    return (
      <Paper className={styles.root}>
        <img className={styles.profilePicture}
          src={this.props.profilePictureSrc} />
        {this.getUploadButton()}
      </Paper>
    );
  }
}