'use strict';

import React from 'react';
import PersonalDetails from './personal-details/personal-details.component.jsx';
import AdditionalPersonalDetails
  from './additional-personal-details/additional-personal-details.component.jsx';
import Controls from './controls/controls.component.jsx';
import Privilege from './privilege/privilege.component.jsx';
import CarouselView from '../ListView/CarouselView';
import styles from './profile-page.scss';

export default class ProfilePage extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      changes: {},
      saveStatus: 'loaded' //enum: loaded, loading, failed
    };
  }

  static propTypes = {
    account: React.PropTypes.object.isRequired,
    readOnly: React.PropTypes.bool,
    save: React.PropTypes.func.isRequired,
    canDelete: React.PropTypes.bool,
    delete: React.PropTypes.func.isRequired,
    canChangePassword: React.PropTypes.bool,
    changePassword: React.PropTypes.func.isRequired,
    canChangePrivilege: React.PropTypes.bool,
    uploadProfilePic: React.PropTypes.func.isRequired,
    onChange: React.PropTypes.func.isRequired,
    cancel: React.PropTypes.func
  }

  static defaultProps = {
    readOnly: false,
    canDelete: true,
    canChangePassword: true,
    canChangePrivilege: true
  }

  handleSave = () => {
    this.setState({saveStatus: 'loading'});
    this.props.save(this.state.changes, (err) => {
      if (err) {
        this.setState({saveStatus: 'failed'});
      } else {
        this.setState({saveStatus: 'loaded'});
      }
    });
  }

  handleCancel = () => {
    // OLD:
    // discard changes
    // this.setState({ changes: {} });
    // show some sort of message

    // let Relay handle this
    this.props.cancel();
  }

  handleChangePassword = () => {
    var options = {}; //what?
    this.props.changePassword(options, (err) => {
      // should show loading and disable buttons
      // while updating password
    });
  }

  handleUploadProfilePic = () => {
    var blob = ''; //what?
    this.props.uploadProfilePic(blob, (err, filename) => {
      // should show loading and disable buttons
      // while saving
      // if !error, filename would be the url of the uploaded pic
    });
  }

  handleChange = (changes) => {
    // NEW: do old code first, to add to this.state.changes, then call onChange 
    // OLD
    // add field-value pairs to this.state.changes
    var newChanges = this.state.changes;

    for (var field in changes) {
      var value = changes[field];

      newChanges[field] = value;
    }

    this.setState({ changes: newChanges });

    // NEW: call onChange
    this.props.onChange(changes);
  }

  render() {
    return (
      <div className={styles.root}>
        <PersonalDetails
          account={this.props.account}
          onChange={this.handleChange}
          uploadProfilePic={this.handleUploadProfilePic}
          saveStatus={this.state.saveStatus}
          readOnly={this.props.readOnly} />
        <AdditionalPersonalDetails
          account={this.props.account}
          onChange={this.handleChange}
          saveStatus={this.state.saveStatus}
          readOnly={this.props.readOnly} />
        <Privilege
          account={this.props.account}
          onChange={this.handleChange}
          sourceRoles={this.props.sourceRoles}
          sourceViewFilters={this.props.sourceViewFilters}
          sourceExemptionFilters={this.props.sourceExemptionFilters}
          saveStatus={this.state.saveStatus}
          readOnly={this.props.readOnly} />
        <CarouselView>
        </CarouselView>
        <Controls
          saveStatus={this.state.saveStatus}
          changePassword={this.props.changePassword}
          save={this.handleSave}
          delete={this.props.delete}
          cancel={this.handleCancel}
          id={this.props.account._id}
          readOnly={this.props.readOnly} />
      </div>
    );
  }
}