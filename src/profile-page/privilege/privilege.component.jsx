'use strict';

import React from 'react';
import TagList from '../../ListView/TagList';
import styles from './privilege.scss';

export default class Privilege extends React.Component {
  constructor(props) {
    super(props);
  }

  static propTypes = {
    account: React.PropTypes.object.isRequired,
    saveStatus: React.PropTypes.oneOf(['loaded', 'loading', 'failed']),
    readOnly: React.PropTypes.bool,
    onChange: React.PropTypes.func.isRequired,
    sourceRoles: React.PropTypes.arrayOf(React.PropTypes.object).isRequired,
    sourceViewFilters: React.PropTypes.arrayOf(React.PropTypes.object),
    sourceExemptionFilters: React.PropTypes.arrayOf(React.PropTypes.object)
  }

  getIsDisabled = () => {
    return (this.props.saveStatus === 'loading')
      || (this.props.readOnly);
  }

  render() {
    return (
      <div className={styles.root}>
        <TagList tags={this.props.sourceRoles} />
        <TagList tags={this.props.sourceViewFilters} />
        <TagList tags={this.props.sourceExemptionFilters} />
      </div>
    );
  }
}