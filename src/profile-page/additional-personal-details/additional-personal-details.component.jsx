'use strict';

import React from 'react';
import injectTapEventPlugin from 'react-tap-event-plugin';
import styles from './additional-personal-details.scss';
import SelectField from 'material-ui/SelectField';
import MenuItem from 'material-ui/MenuItem';
import DatePicker from 'material-ui/DatePicker';

export default class AdditionalPersonalDetails extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      changes: {}
    };
  }

  static propTypes = {
    account: React.PropTypes.object.isRequired,
    saveStatus: React.PropTypes.oneOf(['loaded', 'loading', 'failed']),
    readOnly: React.PropTypes.bool,
    onChange: React.PropTypes.func.isRequired
  }

  static defaultProps = {
    saveStatus: 'loaded',
    readOnly: false
  }

  componentWillMount() {
    try {
      injectTapEventPlugin();
    } catch (err) {
      // in case injectTapEventPlugin was already called (hot reload issue)
    }
  }

  componentDidMount() {
    this.handleUpdate();
  }

  componentDidUpdate() {
    this.handleUpdate();
  }

  handleUpdate = () => {
    // don't update if there are no changes to prevent infinite render loop
    if (Object.keys(this.state.changes).length !== 0) {
      this.props.onChange(this.state.changes);

      this.setState({ changes: {} });
    }
  }

  // make better
  handleSelectMaritalStatus = (event, index, value) => {
    var newChanges = this.state.changes;

    newChanges['maritalStatus'] = value;
    this.setState(newChanges);
  }

  handleSelectSex = (event, index, value) => {
    var newChanges = this.state.changes;

    newChanges['sex'] = value;
    this.setState(newChanges);
  }

  handleSelectBirthdate = (event, date) => {
    var newChanges = this.state.changes;

    newChanges['birthdate'] = date;
    this.setState(newChanges);
  }

  getIsDisabled = () => {
    return (this.props.saveStatus === 'loading')
      || (this.props.readOnly);
  }

  render() {
    return (
      <div className={styles.root}>
        <div className={styles.dropdownPickers}>
          <div className={styles.maritalStatusPicker}>
            <SelectField
              className={styles.select}
              value={this.state.maritalStatus}
              floatingLabelText='Marital Status'
              onChange={this.handleSelectMaritalStatus}
              disabled={this.getIsDisabled()}>
              <MenuItem value='single' primaryText='Single' />
              <MenuItem value='married' primaryText='Married' />
              <MenuItem value='separated' primaryText='Separated' />
              <MenuItem value='divorced' primaryText='Divorced' />
              <MenuItem value='widowed' primaryText='Widowed' />
            </SelectField>
          </div>
          <div className={styles.sexPicker}>
            <SelectField
              className={styles.select}
              value={this.state.sex}
              floatingLabelText={'Sex'}
              onChange={this.handleSelectSex}
              disabled={this.getIsDisabled()}>
              <MenuItem value='male' primaryText='Male' />
              <MenuItem value='female' primaryText='Female' />
            </SelectField>
          </div>
        </div>
        <DatePicker
          className={styles.birthdatePicker}
          value={this.state.birthdate}
          onChange={this.handleSelectBirthdate}
          floatingLabelText='Birthdate'
          container='inline'
          autoOk={true}
          disabled={this.getIsDisabled()} />
      </div>
    );
  }
}