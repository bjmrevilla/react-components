export const testAccount = {
  _id: '0',
  username: '',
  email: '',
  firstName: '',
  middleName: '',
  lastName: '',
  postalCode: '',
  street1: '',
  street2: '',
  municipality: '',
  province: '',
  region: '',
  country: 'Philippines',
  pic: '',
  isProgramStaff: true,
  maritalStatus: 'single',
  sex: 'male'
};

export const testOnChange = (changes) => {
  for (var key in changes) {
    var value = changes[key];

    testAccount[key] = value; // mimic mutation
  }
};

export const testSave = () => {};
export const testDelete = () => {};
export const testChangePassword = () => {};
export const testUploadProfilePic = () => {};
export const testCancel = () => {};

export const testSourceRoles = [
  {key: 'role 1'},
  {key: 'role 2'},
  {key: 'role 3'}
];

export const testSourceViewFilters = [
  {key: 'view filter 1'},
  {key: 'view filter 2'},
  {key: 'view filter 3'}
];

export const testSourceExemptionFilters = [
  {key: 'exemption 1'},
  {key: 'exemption 2'},
  {key: 'exemption 3'}
];