import React from 'react';
import { storiesOf, action } from '@kadira/storybook';
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';
import PersonalDetails from '../personal-details';
import {testAccount, testOnChange} from './fixtures';

storiesOf('ProfilePage',module)
  .add('personalDetails', () => (
    <MuiThemeProvider>
      <PersonalDetails account={testAccount} onChange={testOnChange} saveStatus='loading' />
    </MuiThemeProvider>
  ));