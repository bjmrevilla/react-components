import React from 'react';
import { storiesOf, action } from '@kadira/storybook';
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';
import ProfilePage from '../profile-page.component.jsx';
import {testAccount,
  testOnChange,
  testSave,
  testDelete,
  testChangePassword,
  testUploadProfilePic,
  testCancel,
  testSourceRoles,
  testSourceViewFilters,
  testSourceExemptionFilters} from './fixtures';

storiesOf('ProfilePage',module)
  .add('component', () => (
    <MuiThemeProvider>
      <ProfilePage account={testAccount}
        onChange={testOnChange}
        save={testSave}
        delete={testDelete}
        changePassword={testChangePassword}
        uploadProfilePic={testUploadProfilePic}
        cancel={testCancel}
        sourceRoles={testSourceRoles}
        sourceViewFilters={testSourceViewFilters}
        sourceExemptionFilters={testSourceExemptionFilters} />
    </MuiThemeProvider>
  ));