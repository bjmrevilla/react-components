'use strict';

import React from 'react';
import RoleSettings from '../role-settings/role-settings.component.jsx';
import AdminRoleSettings
  from '../role-settings/admin-role-settings.component.jsx';
import ProgramStaffRoleSettings
  from '../role-settings/program-staff-role-settings.component.jsx';
import RoleControls from './role-controls.component.jsx';
import styles from './roles-settings.scss';

export default class RolesSettings extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      changes: {},
      saveStatus: 'loaded', //enum: loaded, loading, failed
      edit: false,
      roleIndex: 0
    };
  }

  static propTypes = {
    roles: React.PropTypes.arrayOf(React.PropTypes.object).isRequired,
    save: React.PropTypes.func.isRequired,
    type: React.PropTypes.oneOf(['admin', 'programStaff']).isRequired,
    adminRoles: React.PropTypes.arrayOf(React.PropTypes.object),
    programStaffRoles: React.PropTypes.arrayOf(React.PropTypes.object)
  }

  handleChange = (changes) => {
    //TODO: fix this to add indication of role changes
    // add field-value pairs to this.state.changes
    var newChanges = this.state.changes;
    var roleChanges = {};

    for (var field in changes) {
      var value = changes[field];

      // roleChanges[field] = value;
      newChanges[field] = value;
    }

    this.setState({ changes: newChanges });
  }

  onRoleChange = (event, index, value) => {
    this.setState({roleIndex: value, changes: {}, edit: false});
  }

  handleCancel = () => {
    this.setState({edit: false, changes: {}});
  }
  
  handleEdit = () => {
    this.setState({edit: true});
  }

  handleSave = () => {
    // set saveStatus, to show loading and disable buttons
    this.setState({saveStatus: 'loading'});
    this.props.save(this.state.changes, (err) => {
      // should show loading and disable buttons
      // while saving
      if (err) {
        this.setState({saveStatus: 'failed'});
      } else {
        this.setState({
          changes: {},
          edit: false,
          saveStatus: 'loaded'});
      }
    });
  }

  render() {
    return (
      <div className={styles.root}>
        <RoleControls
          roles={this.props.roles}
          roleIndex={this.state.roleIndex}
          onRoleChange={this.onRoleChange}
          handleEdit={this.handleEdit}
          edit={this.state.edit}
          saveStatus={this.state.saveStatus}
          handleCancel={this.handleCancel}
          handleSave={this.handleSave} />
        <RoleSettings
          type={this.props.type}
          role={this.props.roles[this.state.roleIndex]}
          onChange={this.handleChange}
          edit={this.state.edit}
          adminRoles={this.props.adminRoles}
          programStaffRoles={this.props.programStaffRoles} />
      </div>
    );
  }
}