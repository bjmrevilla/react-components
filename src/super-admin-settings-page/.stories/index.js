import React from 'react';
import { storiesOf, action } from '@kadira/storybook';
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';
import SuperAdminSettingsPage from '../super-admin-settings-page.component.jsx';
import {testAdminRoles, testProgramStaffRoles} from './fixtures';

storiesOf('SuperAdminSettingsPage',module)
  .add('component', () => (
    <MuiThemeProvider>
      <SuperAdminSettingsPage
        adminRoles={testAdminRoles}
        programStaffRoles={testProgramStaffRoles} />
    </MuiThemeProvider>
  ));