import React from 'react';
import { storiesOf, action } from '@kadira/storybook';
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';
import ProgramStaffRoleSettings
  from '../role-settings/program-staff-role-settings.component.jsx';
import {testRoles} from './fixtures';

storiesOf('SuperAdminSettingsPage',module)
  .add('program.staff.role.settings', () => (
    <MuiThemeProvider>
      <ProgramStaffRoleSettings role={testRoles[0]} />
    </MuiThemeProvider>
  ));