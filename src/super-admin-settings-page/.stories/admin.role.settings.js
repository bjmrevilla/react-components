import React from 'react';
import { storiesOf, action } from '@kadira/storybook';
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';
import AdminRoleSettings
  from '../role-settings/admin-role-settings.component.jsx';
import {testRoles} from './fixtures';

storiesOf('SuperAdminSettingsPage',module)
  .add('admin.role.settings', () => (
    <MuiThemeProvider>
      <AdminRoleSettings role={testRoles[0]} />
    </MuiThemeProvider>
  ));