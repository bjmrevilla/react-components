'use strict';

import React from 'react';
import Section from './section/section.component.jsx';
import styles from './super-admin-settings-page.scss';

export default class SuperAdminSettingsPage extends React.Component {
  constructor(props) {
    super(props);
  }

  static propTypes = {
    adminRoles: React.PropTypes.arrayOf(React.PropTypes.object)
      .isRequired,
    programStaffRoles: React.PropTypes.arrayOf(React.PropTypes.object)
      .isRequired,
    save: React.PropTypes.func.isRequired
  }

  render() {
    return (
      <div className={styles.root}>
        <h1 className={styles.title}>
          Settings
        </h1>
        <Section
          type='admin'
          roles={this.props.adminRoles}
          save={this.props.save}
          adminRoles={this.props.adminRoles}
          programStaffRoles={this.props.programStaffRoles} />
        <Section
          type='programStaff'
          roles={this.props.programStaffRoles}
          save={this.props.save} />
      </div>
    );
  }
}