'use strict';

import React from 'react';
import TagsSetting from './tags-setting.component.jsx';
import Section from './section.component.jsx';
import styles from './role-settings.scss';

export default class ProgramStaffRoleSettings extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      changes: {}
    };
  }

  static propTypes = {
    role: React.PropTypes.object.isRequired,
    onChange: React.PropTypes.func.isRequired,
    edit: React.PropTypes.bool,
    tags: React.PropTypes.arrayOf(React.PropTypes.object)
      .isRequired
  }

  static defaultProps = {
    edit: false
  }

  componentDidMount() {
    this.handleUpdate();
  }

  componentDidUpdate() {
    this.handleUpdate();
  }

  handleUpdate = () => {
    // don't update if there are no changes to prevent infinite render loop
    if (Object.keys(this.state.changes).length !== 0) {
      this.props.onChange(this.state.changes);

      this.setState({ changes: {} });
    }
  }

  handleChange = (field, value) => {
    // append field-value pair instead of creating new object altogether
    var newChanges = this.state.changes;
    newChanges[field] = value;

    this.setState(newChanges);
  }

  getTagsSettings = () => {
    var settingsArrangement =
    [{description: 'I can view materials with tags:',
      setting: this.props.role.canViewMaterialWithTag},
     {description: 'I am exempted to assessment on materials with tags:',
      setting: this.props.role.isExemptedToExamsOnMaterialsWithTag}];
    
    return settingsArrangement.map((tagsSetting, index) => {
      return (
        <TagsSetting
          key={index}
          description={tagsSetting.description}
          setting={tagsSetting.setting}
          onChange={this.handleChange}
          disabled={!this.props.edit} />
      );
    });
  }

  render() {
    return (
      <div className={styles.root}>
        <Section title='Manuals'>
          {this.getTagsSettings() }
        </Section>
      </div>
    );
  }
}