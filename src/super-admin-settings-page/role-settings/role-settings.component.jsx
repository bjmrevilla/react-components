'use strict';

import React from 'react';
import AdminRoleSettings
  from './admin-role-settings.component.jsx';
import ProgramStaffRoleSettings
  from './program-staff-role-settings.component.jsx';

export default class RoleSettings extends React.Component {
  constructor(props) {
    super(props);
  }

  static propTypes = {
    type: React.PropTypes.oneOf(['admin', 'programStaff']).isRequired,
    role: React.PropTypes.object.isRequired,
    onChange: React.PropTypes.func.isRequired,
    edit: React.PropTypes.bool,
    adminRoles: React.PropTypes.arrayOf(React.PropTypes.object),
    programStaffRoles: React.PropTypes.arrayOf(React.PropTypes.object)
  }

  static defaultProps = {
    edit: false
  }

  getRoleSettings = () => {
    if (this.props.type === 'admin') {
      return <AdminRoleSettings {...this.props} />;
    }
    if (this.props.type === 'programStaff') {
      return <ProgramStaffRoleSettings {...this.props} />;
    }
  }

  render() {
    return (
      <div>
        {this.getRoleSettings()}
      </div>
    );
  }
}