'use strict';

import React from 'react';
import TagList from '../../ListView/TagList';
import styles from './tags-setting.scss';

export default class TagsSetting extends React.Component {
  constructor(props) {
    super(props);
  }

  static propTypes = {
    description: React.PropTypes.string.isRequired,
    setting: React.PropTypes.arrayOf(React.PropTypes.object).isRequired,
    onChange: React.PropTypes.func.isRequired,
    disabled: React.PropTypes.bool
  }

  render() {
    return (
      <div className={styles.root}>
        <div className={styles.description}>
          {this.props.description}
        </div>
        <TagList
          tags={this.props.setting}
          title={this.props.description} />
      </div>
    );
  }
}