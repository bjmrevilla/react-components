'use strict';

import React from 'react';
import TagsSetting from './tags-setting.component.jsx';
import Section from './section.component.jsx';
import styles from './role-settings.scss';
import Toggle from 'material-ui/Toggle';

export default class AdminRoleSettings extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      changes: {}
    };
  }

  static propTypes = {
    role: React.PropTypes.object.isRequired,
    onChange: React.PropTypes.func.isRequired,
    edit: React.PropTypes.bool,
    adminRoles: React.PropTypes.arrayOf(React.PropTypes.object)
      .isRequired,
    programStaffRoles: React.PropTypes.arrayOf(React.PropTypes.object)
      .isRequired
  }

  static defaultProps = {
    edit: false
  }

  componentDidMount() {
    this.handleUpdate();
  }

  componentDidUpdate() {
    this.handleUpdate();
  }

  handleUpdate = () => {
    // don't update if there are no changes to prevent infinite render loop
    if (Object.keys(this.state.changes).length !== 0) {
      this.props.onChange(this.state.changes);

      this.setState({ changes: {} });
    }
  }

  handleChange = (field, value) => {
    // append field-value pair instead of creating new object altogether
    var newChanges = this.state.changes;
    newChanges[field] = value;

    this.setState(newChanges);
  }
  
  getAdminTagsSettings = () => {
    //assumed properties are hidden in this.props.role
    //set this up, because I have to hardcode one way or another
    var settingsArrangement =
    [{description: 'I can view admins with roles:',
      setting: this.props.role.canViewAdminWithRoles},
     {description: 'I can edit admins with roles:',
      setting: this.props.role.canEditAdminWithRoles},
     {description: 'I can add these roles to admins I can edit:',
      setting: this.props.role.canAddToAdminRoles},
     {description: 'I can delete admins with roles:',
      setting: this.props.role.canDeleteAdminWithRoles}];
    
    return settingsArrangement.map((tagsSetting, index) => {
      return (
        <TagsSetting
          key={index}
          description={tagsSetting.description}
          setting={tagsSetting.setting}
          onChange={this.handleChange}
          disabled={!this.props.edit} />
      );
    });
  }

  getProgramStaffTagsSettings = () => {
    var settingsArrangement =
    [{description: 'I can view programStaff with roles:',
      setting: this.props.role.canViewProgramStaffWithRoles},
     {description: 'I can edit programStaff with roles:',
      setting: this.props.role.canEditProgramStaffWithRoles},
     {description: 'I can add these roles to programStaff I can edit:',
      setting: this.props.role.canAddToProgramStaffRoles},
     {description: 'I can delete programStaff with roles:',
      setting: this.props.role.canDeleteProgramStaffWithRoles}];
    
    return settingsArrangement.map((tagsSetting, index) => {
      return (
        <TagsSetting
          key={index}
          description={tagsSetting.description}
          setting={tagsSetting.setting}
          onChange={this.handleChange}
          disabled={!this.props.edit} />
      );
    });
  }

  getManualCreationSettings = () => {
    var settingArrangement =
    {description: 'Can create a manual',
     setting: this.props.role.canCreateManual};

    return (
      <Toggle
        label={settingArrangement.description}
        value={settingArrangement.setting}
        onChange={this.handleChange}
        disabled={!this.props.edit} />
    );    
  }

  getCurriculumCreationSettings = () => {
    var settingArrangement =
    {description: 'Can create a curriculum',
     setting: this.props.role.canCreateCurriculum};

    return (
      <Toggle
        label={settingArrangement.description}
        value={settingArrangement.setting}
        onChange={this.handleChange}
        disabled={!this.props.edit} />
    );    
  }

  render() {
    return (
      <div className={styles.root}>
        <Section title='Manual Creation'>
          {this.getManualCreationSettings() }
        </Section>
        <Section title='Curriculum Creation'>
          {this.getCurriculumCreationSettings() }
        </Section>
        <Section title='Admin accounts'>
          {this.getAdminTagsSettings() }
        </Section>
        <Section title='ProgramStaff accounts'>
          {this.getProgramStaffTagsSettings() }
        </Section>
      </div>
    );
  }
}