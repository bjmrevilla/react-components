'use strict';

import React from 'react';
import View from './view/view.component.jsx';
import styles from './statistics.scss';

export default class Statistics extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      rootWidth: 0
    }
  }

  static propTypes = {
    statistics: React.PropTypes.arrayOf(React.PropTypes.shape({
      displayType: React.PropTypes.oneOf(
        ['bar', 'pie', 'line', 'area', 'scatterplot']),
      displaySize: React.PropTypes.oneOf(
        ['normal', 'wide', 'small']),
      title: React.PropTypes.string,
      description: React.PropTypes.string,
      data: React.PropTypes.arrayOf(React.PropTypes.object),
      xLabel: React.PropTypes.string,
      xKey: React.PropTypes.arrayOf(React.PropTypes.string),
      yLabel: React.PropTypes.string,
      yKey: React.PropTypes.any,
      typeLabel: React.PropTypes.string,
      typeKey: React.PropTypes.arrayOf(React.PropTypes.string),
      showLegend: React.PropTypes.bool
    }))
  }

  componentDidMount() {
    setTimeout(this.getRootMeasure);
  }

  // I need this to dynamically size react-easy-chart's Charts
  getRootMeasure = () => {
    this.setState({rootWidth: this.refs.rootDiv.clientWidth});
  }

  // I need to reorder this.props.statistics so that
  // I don't get ugly empty spaces when I render, usually caused by
  // different combinations of 'normal' and 'wide' elements
  reorderArray = (array) => {
    var newArray = [];
    var newTotalArea = 0;
    var startWideCount = 0;
    var hasEncounteredFirstNonWide = false;
    var getArea = (element) => (element.displaySize === 'small')
      ? 1
      : (element.displaySize === 'normal')
        ? 4
        : 8;
    var pushToStart = (array, element) => [element].concat(array);
    var pushNormalToStart = (array, element) => {
      //I can't push a 'normal' element to before a wide element
      if (startWideCount === 0) {
        return pushToStart(array, element);
      } else {
        return array.slice(0, startWideCount)
          .concat([element])
          .concat(array.slice(startWideCount));
      }
    }

    for (var i in array) {
      var element = array[i];
      if (!hasEncounteredFirstNonWide
        && element.displaySize === 'wide') {
          startWideCount++;
      }

      if ((element.displaySize === 'normal')
        && (newTotalArea % 4 !== 0)) {
          newArray = pushNormalToStart(newArray, element);
      } else if ((element.displaySize === 'wide')
        && (newTotalArea % 8 !== 0)) {
          newArray = pushToStart(newArray, element);
          startWideCount++;
      } else {
        newArray.push(element);
      }

      newTotalArea += getArea(element);
    }

    return newArray;
  }

  getViews = () => {
    var statistics = this.reorderArray(this.props.statistics);
    return statistics.map((statistic) => {
      return (
        <View displayType={statistic.displayType}
          displaySize={statistic.displaySize}
          rootWidth={this.state.rootWidth}
          rootHeight={this.state.rootHeight}
          title={statistic.title}
          description={statistic.description}
          data={statistic.data}
          xLabel={statistic.xLabel}
          xKey={statistic.xKey}
          yLabel={statistic.yLabel}
          yKey={statistic.yKey} />
      );
    });
  }

  render() {
    return (
      <div ref='rootDiv' className={styles.root}>
      <h1 className={styles.title}>
        Statistics
      </h1>
        {this.getViews()}
      </div>
    );
  }
}