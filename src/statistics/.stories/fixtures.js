export const testScatterplot = {
  displayType: 'scatterplot',
  displaySize: 'small',
  title: 'My Scatterplot',
  description: 'number of points on this graph vs length of this graph',
  data: [{x: 0, y: 0},
    {x: 1, y: 1},
    {x: 2, y: 2}],
  xLabel: 'length of this graph',
  xKey: ['x'],
  yLabel: 'number of points on this graph',
  yKey: ['y'],
  showLegend: false
};

export const testBar = {
  displayType: 'bar',
  displaySize: 'small',
  title: 'My Bar Chart',
  description: 'test bars',
  data: [
      {x: 'A', y: 20},
      {x: 'B', y: 30},
      {x: 'C', y: 40},
      {x: 'D', y: 20},
      {x: 'E', y: 40},
      {x: 'F', y: 25},
      {x: 'G', y: 5}
    ],
  xLabel: 'some letters',
  xKey: ['x'],
  yLabel: 'some numbers',
  yKey: ['y']
};

export const testPie = {
  displayType: 'pie',
  displaySize: 'small',
  title: 'lolPie',
  description: 'number of points on this graph vs length of this graph',
  data: [
    { key: 'A', value: 100 },
    { key: 'B', value: 200 },
    { key: 'C', value: 50 }
  ],
  xLabel: 'length of this graph',
  xKey: ['x'],
  yLabel: 'number of points on this graph',
  yKey: ['y'],
  showLegend: true
};

export const testLine = {
  displayType: 'line',
  displaySize: 'small',
  title: 'Lines',
  description: 'these are some lines',
  data: [
      [
        { x: 1, y: 20 },
        { x: 2, y: 10 },
        { x: 3, y: 25 }
      ]
    ],
  xLabel: 'X',
  xKey: ['x'],
  yLabel: 'Y',
  yKey: ['y']
};

export const testArea = {
  displayType: 'area',
  displaySize: 'small',
  title: 'My Area',
  description: 'check out this area',
  data: [
      [
        { x: 1, y: 20 },
        { x: 2, y: 10 },
        { x: 3, y: 25 }
      ]
    ],
  xLabel: 'AreaX',
  xKey: ['x'],
  yLabel: 'AreaY',
  yKey: ['y']
};

export const testStatistics = [testScatterplot,
  testBar,
  testPie,
  testLine,
  testArea];