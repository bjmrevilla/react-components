import React from 'react';
import { storiesOf, action } from '@kadira/storybook';
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';
import Statistics from '../statistics.component.jsx';
import {testStatistics} from './fixtures';

storiesOf('Statistics',module)
  .add('component', () => (
    <MuiThemeProvider>
      <Statistics statistics={testStatistics} />
    </MuiThemeProvider>
  ));