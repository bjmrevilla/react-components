export const accounts = [
    { _id: '1', username: 'Mom', email: 'cool.mom@hotmail.com', pic: '' },
    { _id: '2', username: 'Dad', email: 'dad@gmail.com', pic: '' },
    { _id: '3', username: 'Caesar', email: 'caesar.is@live.com', pic: '' }
];

export const currentAccount = { _id: '0', username: 'Me', email: 'me@gmail.com', pic: '' };

export const message1 = {
    body: 'hey man',
    date: 'Just now',
    creator: currentAccount
};

export const message2 = {
    body: 'hey',
    date: '',
    creator: accounts[2]
};

export const longMessage = {
    body: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam at molestie est. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Mauris tempor tortor consequat scelerisque posuere. Curabitur ut felis sit amet sapien commodo dictum. In tempus, ipsum euismod vulputate auctor, velit diam vulputate ligula, in sodales libero nulla eget nisl. Sed tristique, urna ut faucibus semper, tellus enim mollis ante, in iaculis urna turpis at odio. Vivamus gravida sem elit, sed finibus ligula tempus non. Duis est tortor, commodo et justo ut, ullamcorper dictum sapien.',
    date: '1500 years ago',
    creator: accounts[2]
};

export const messages = [longMessage, message2, message1];

export const room1 = {
    _id: '1', title: 'Discussing Literature', tags: 'with Caesar',
    messages: messages
};

export const rooms = [
    room1,
    { _id: '2', title: 'Group Project', tags: 'Noone else in chat' },
    { _id: '3', title: 'Family', tags: 'with Mom and Dad' }
];

//mock callbacks for testing
export const search = (filter, cb) => {};
export const createRoom = (cb) => {};
export const sendMessage = (message, cb) => {};
export const updateRoomDetails = (changes, cb) => {};