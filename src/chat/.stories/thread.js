import React from 'react'
import { storiesOf, action } from '@kadira/storybook'
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider'
import Thread from '../thread'
import {
  accounts,
  currentAccount,
  longMessage,
  messages,
  room1,
  rooms
} from './fixtures'

storiesOf('Chat', module)
  .add('thread', () => {
    const room1 = {_id: '1', title: 'Discussing Literature', tags: 'with Caesar', messages: messages};
    const sendMessage = (message, cb) => {
      setTimeout(() => {
        //ff: test connection
        room1.messages = room1.messages.concat([message]);
        cb();

        //ff: test disconnection
        // cb(new Error());
      }, 3000);
    };
    var updateRoomDetails = (changes, cb) => {};

    return (
      <MuiThemeProvider>
        <Thread
          room={room1}
          currentAccount={currentAccount}
          sendMessage={sendMessage}
          updateRoomDetails={updateRoomDetails}
        />
      </MuiThemeProvider>
    )
  })
