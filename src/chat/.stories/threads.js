import React from 'react'
import { storiesOf, action } from '@kadira/storybook'
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider'
import Threads from '../threads'
import {
  accounts,
  currentAccount,
  longMessage,
  messages,
  room1,
  rooms
} from './fixtures'

storiesOf('Chat', module)
  .add('threads', () => {
    return (
      <MuiThemeProvider>
        <Threads
          rooms={rooms}
          accounts={accounts}
          search={action('search')}
          createRoom={action('create room')}
        />
      </MuiThemeProvider>
    )
  })
