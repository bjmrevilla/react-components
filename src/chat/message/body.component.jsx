'use strict';

import React from 'react';
import styles from './body.scss';

export default class Body extends React.Component {
  constructor(props) {
    super(props);
  }

  static propTypes = {
    body: React.PropTypes.string.isRequired
  };

  render() {
    return (
      <div className={styles.root}>
        {this.props.body}
      </div>
    );
  }
}
