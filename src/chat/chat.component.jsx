'use strict';

import React from 'react';
import Thread from './thread/thread.component.jsx';
import Threads from './threads/threads.component.jsx';
import styles from './chat.scss';

export default class Chat extends React.Component {
  constructor(props) {
    super(props);

    this.filterAccounts = this.filterAccounts.bind(this);
  }

  static propTypes = {
    rooms: React.PropTypes.arrayOf(React.PropTypes.object).isRequired,
    accounts: React.PropTypes.arrayOf(React.PropTypes.object).isRequired,
    currentAccount: React.PropTypes.object.isRequired,
    search: React.PropTypes.func.isRequired,
    createRoom: React.PropTypes.func.isRequired,
    sendMessage: React.PropTypes.func.isRequired,
    updateRoomDetails: React.PropTypes.func.isRequired
  }

  filterAccounts(filter, accounts) {
    var filtered = [];
    filter = filter.toLowerCase();

    for (var i in accounts) {
      var account = accounts[i];

      if (account.username.toLowerCase().indexOf(filter) >= 0) {
        filtered.push(account);
      }
    }

    return filtered;
  }

  render = () => {
    return (
      <div className={styles.root}>
        <div className={styles.threads}>
          <Threads rooms={this.props.rooms}
            accounts={this.props.accounts}
            search={this.props.search}
            createRoom={this.props.createRoom} />
        </div>
        <hr className={styles.separator} />
        <div className={styles.thread}>
          <Thread room={this.props.room}
            currentAccount={this.props.currentAccount}
            sendMessage={this.props.sendMessage}
            updateRoomDetails={this.props.updateRoomDetails} />
        </div>
      </div>
    );
  }
}
