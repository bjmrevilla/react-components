import React from 'react';
import ThreadsList from './threads.list.component.jsx';
import Title from './title.component.jsx';
import Search from './search.component.jsx';
import Loading from './loading.component.jsx';
import styles from './threads.scss';

export default class Threads extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      searchText: '',
      accountsOngoingRequests: 0,
      roomsOngoingRequests: 0,
      accountsRequestFailed: false,
      roomsRequestFailed: false
    };
  }

  static propTypes = {
    rooms: React.PropTypes.arrayOf(React.PropTypes.object).isRequired,
    accounts: React.PropTypes.arrayOf(React.PropTypes.object).isRequired,
    search: React.PropTypes.func.isRequired,
    createRoom: React.PropTypes.func.isRequired
  }

  onSearch = (e) => {
    this.setState({
      searchText: e.target.value,
      accountsOngoingRequests: this.state.accountsOngoingRequests + 1
    },
      () => {
        this.props.search(this.state.searchText, (err) => {
          // watch (err instanceof Error) for possible bugs
          this.setState({
            accountsRequestFailed: (err instanceof Error),
            accountsOngoingRequests: this.state.accountsOngoingRequests - 1
          });
        });
      });
  }

  onCreateRoom = () => {
    this.setState({
      searchText: '',
      roomsOngoingRequests: this.state.roomsOngoingRequests + 1
    });

    this.props.createRoom((err) => {
      // watch (err instanceof Error) for possible bugs
      this.setState({
        roomsRequestFailed: (err instanceof Error),
        roomsOngoingRequests: this.state.roomsOngoingRequests - 1
      });
    });
  }

  getItems = () => {
    return (this.state.searchText === '')
      ? this.props.rooms
      : this.props.accounts;
  }

  getType = () => {
    return (this.state.searchText === '')
      ? 'room'
      : 'account';
  }

  getList = () => {
    var requestsLoading = (this.state.searchText === '')
      ? this.state.roomsOngoingRequests
      : this.state.accountsOngoingRequests;

    var requestFailed = (this.state.searchText === '')
      ? this.state.roomsRequestFailed
      : this.state.accountsRequestFailed;

    // if requests are still loading, show loading animation
    // regardless of success or failure of previous requests
    var loadStatus = (requestsLoading !== 0)
      ? 'loading'
      : (requestFailed)
        ? 'failed'
        : 'loaded';

    return (loadStatus === 'loaded')
      ? <ThreadsList items={this.getItems() } type={this.getType() }
        onCreateRoom={this.onCreateRoom} />
      : <Loading loadStatus={loadStatus} />;
  }

  render() {
    return (
      <div className={styles.root}>
        <Search searchText={this.state.searchText}
          onSearch={this.onSearch} />
        <Title type={this.getType() } />
        {this.getList() }
      </div>
    );
  }
}