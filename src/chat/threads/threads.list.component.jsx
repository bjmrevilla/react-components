'use strict';

import React from 'react';
import styles from './threads.list.scss';
import {List, ListItem} from 'material-ui/List';
import Avatar from 'material-ui/Avatar';

export default class ThreadsList extends React.Component {
  constructor(props) {
    super(props);
  }

  static propTypes = {
    items: React.PropTypes.arrayOf(React.PropTypes.object).isRequired,
    type: React.PropTypes.oneOf(['room', 'account']).isRequired,
    onCreateRoom: React.PropTypes.func.isRequired
  }

  getPrimaryText = (item) => {
    if (this.props.type === 'room') {
      return item.title;
    }
    if (this.props.type === 'account') {
      return item.username;
    }

    return '';
  }

  getSecondaryText = (item) => {
    if (this.props.type === 'room') {
      return item.tags;
    }
    if (this.props.type === 'account') {
      return item.email;
    }

    return '';
  }

  getItemPicture = (item) => {
    if (this.props.type === 'room') {
      return '';
    }
    if (this.props.type === 'account') {
      return item.pic;
    }

    return '';
  }

  getOnClick = () => {
    if (this.props.type === 'room') {
      return () => { };
    }
    if (this.props.type === 'account') {
      return this.props.onCreateRoom;
    }

    return '';
  }

  getListItems = () => {
    return this.props.items.map((item) => {
      return (
        <ListItem key={item._id}
          primaryText={this.getPrimaryText(item) }
          secondaryText={this.getSecondaryText(item) }
          leftAvatar={<Avatar src={this.getItemPicture(item) } />}
          onClick={this.getOnClick() } />
      );
    });
  }

  render() {
    return (
      <div className={styles.root}>
        <List>
          {this.getListItems() }
        </List>
      </div>
    );
  }
}