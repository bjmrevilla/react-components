'use strict';

import React from 'react';
import styles from './title.scss';

export default class Title extends React.Component {
  constructor(props) {
    super(props);
  }

  static propTypes = {
    type: React.PropTypes.oneOf(['room', 'account']).isRequired
  }

  getTitle() {
    if (this.props.type === 'room') {
      return 'Threads';
    }
    if (this.props.type === 'account') {
      return 'Accounts';
    }

    return '';
  }

  render() {
    return (
      <div className={styles.root}>
        <h4 className={styles.title}>
          {this.getTitle() }
        </h4>
      </div>
    );
  }
}