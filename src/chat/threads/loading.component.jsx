'use strict';

import React from 'react';
import styles from './loading.scss';
import CircularProgress from 'material-ui/CircularProgress';

export default class Loading extends React.Component {
  constructor(props) {
    super(props);
  }

  static propTypes = {
    loadStatus: React.PropTypes.oneOf(['loading', 'loaded', 'failed'])
  }

  static defaultProps = {
    loadStatus: 'loaded'
  }

  getLoading = () => {
    if (this.props.loadStatus === 'loading') {
      return (
        <CircularProgress />
      );
    }
    if (this.props.loadStatus === 'loaded') {
      return '';
    }
    if (this.props.loadStatus === 'failed') {
      return (
        <span className={styles.failedText}>
          Can't connect to the Internet right now.
        </span>
      );
    }
  }

  render() {
    return (
      <div className={styles.root}>
        {this.getLoading() }
      </div>
    );
  }
}