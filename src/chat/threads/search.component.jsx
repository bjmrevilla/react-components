'use strict';

import React from 'react';
import styles from './search.scss';
import TextField from 'material-ui/TextField';

export default class Search extends React.Component {
  constructor(props) {
    super(props);
  }

  static propTypes = {
    searchText: React.PropTypes.string.isRequired,
    onSearch: React.PropTypes.func.isRequired
  }

  render() {
    return (
      <div className={styles.root}>
        <TextField type='text'
          floatingLabelText='Search'
          className={styles.searchBox}
          value={this.props.searchText}
          onChange={this.props.onSearch}
          fullWidth={true} />
      </div>
    );
  }
}