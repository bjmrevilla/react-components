'use strict';

import React from 'react';
import Messages from './messages.component.jsx';
import Title from './title.component.jsx';
import Send from './send.component.jsx';
import styles from './thread.scss';
import Divider from 'material-ui/Divider';

export default class Thread extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      messageText: '',
      updateRoomStatus: 'loaded',
      sendOngoingRequests: 0,
      sendFailed: false
    };
  }

  static propTypes = {
    currentAccount: React.PropTypes.object.isRequired,
    room: React.PropTypes.object.isRequired,
    sendMessage: React.PropTypes.func.isRequired,
    updateRoomDetails: React.PropTypes.func.isRequired
  }

  onType = (e) => {
    this.setState({ messageText: e.target.value });
  }

  onSend = () => {
    // no whitespace-only messages
    if (/^\s*$/.test(this.state.messageText)) return;
    
    var message = {
      room: this.props.room._id,
      message: this.state.messageText
    };

    this.setState({
      messageText: '',
      sendOngoingRequests: this.state.sendOngoingRequests + 1
    });

    this.props.sendMessage(message, (err) => {
      this.setState({
        sendFailed: (err instanceof Error),
        sendOngoingRequests: this.state.sendOngoingRequests - 1
      });
    });
  }

  onUpdateDetails = () => {
    var changes = {};

    this.setState({ updateRoomStatus: 'loading' });

    this.props.updateRoomDetails(changes, (err) => {
      if (err) {
        this.setState({ updateRoomStatus: 'failed' });
      } else {
        this.setState({ updateRoomStatus: 'loaded' });
      }
    });
  }

  getSendStatus = () => {
    // if requests are still loading, show loading animation
    // regardless of success or failure of previous requests
    return (this.state.sendOngoingRequests !== 0)
      ? 'loading'
      : (this.state.sendFailed)
        ? 'failed'
        : 'loaded';
  }

  render() {
    return (
      <div className={styles.root}>
        <Title title={this.props.room.title} />
        <hr className={styles.separator} />
        <Messages currentAccount={this.props.currentAccount}
          messages={this.props.room.messages}
          sendStatus={this.getSendStatus() } />
        <hr className={styles.separator} />
        <Send message={this.state.messageText} onType={this.onType}
          onSend={this.onSend} />
      </div>
    );
  }
}
