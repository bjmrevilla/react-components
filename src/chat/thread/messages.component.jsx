'use strict';

import React from 'react';
import Message from '../message/message.component.jsx';
import Loading from './loading.component.jsx';
import styles from './messages.scss';

export default class Messages extends React.Component {
  constructor(props) {
    super(props);
  }

  static propTypes = {
    currentAccount: React.PropTypes.object.isRequired,
    messages: React.PropTypes.arrayOf(React.PropTypes.object).isRequired
  }

  getProfilePicturePosition = (messageCreator) => {
    return (messageCreator._id === this.props.currentAccount._id)
      ? 'right'
      : 'left';
  }

  getMessageFlexAlign = (messageCreator) => {
    return (messageCreator._id === this.props.currentAccount._id)
      ? 'flex-end'
      : 'flex-start';
  }

  getMessageList = () => {
    return this.props.messages.map((message) => {
      return (
        <div className={styles.message}
          style={{ alignSelf: this.getMessageFlexAlign(message.creator)}}>
          <Message key={message._id}
            message={message}
            profilePicturePosition={this.getProfilePicturePosition(
              message.creator)} />
        </div>
      );
    });
  }

  componentDidMount = () => {
    setTimeout(() => {
      this.shouldScrollBottom = true;
      var node = this.refs.root;
      
      node.scrollTop = node.scrollHeight;
      
      
    }, 1000); //find out why 1000 is needed
  }

  componentWillUpdate = () => {
    var node = this.refs.root;
    this.shouldScrollBottom = (node.scrollTop + node.offsetHeight ===
      node.scrollHeight);
    
  }

  componentDidUpdate = () => {
    if (this.shouldScrollBottom) {
      var node = this.refs.root;
      node.scrollTop = node.scrollHeight;
    }

    
  }

  render() {
    return (
      <div className={styles.root} ref='root'>
        {this.getMessageList() }
        <Loading className={styles.loading} sendStatus={this.props.sendStatus} />
      </div>
    );
  }
}