'use strict';

import React from 'react';
import styles from './title.scss';

export default class Title extends React.Component {
  constructor(props) {
    super(props);
  }

  static propTypes = {
    title: React.PropTypes.string.isRequired
  }

  render() {
    return (
      <div className={styles.root}>
        <span className={styles.title}>
          {this.props.title}
        </span>
      </div>
    );
  }
}
