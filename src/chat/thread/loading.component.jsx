'use strict';

import React from 'react';
import styles from './loading.scss';
import CircularProgress from 'material-ui/CircularProgress';

export default class Loading extends React.Component {
  constructor(props) {
    super(props);
  }

  static propTypes = {
    sendStatus: React.PropTypes.oneOf(['loading', 'loaded', 'failed'])
  }

  static defaultProps = {
    sendStatus: 'loaded'
  }

  getLoading = () => {
    if (this.props.sendStatus === 'loading') {
      return (
        <CircularProgress size={0.5} />
      );
    }
    if (this.props.sendStatus === 'loaded') {
      return '';
    }
    if (this.props.sendStatus === 'failed') {
      return (
        <span className={styles.failedText}>
          Can't connect to the Internet right now.
        </span>
      );
    }
  }

  render() {
    return (
      <div className={styles.root}>
        {this.getLoading() }
      </div>
    );
  }
}