import React from 'react'
import { compose, withState, withHandlers } from 'recompose'
import { storiesOf, action } from '@kadira/storybook'
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider'
import MaterialCreator from '../index'
import PDFViewer from '../../../Previews/PDFViewer'
import { props as pdfProps } from '../../../Previews/PDFViewer/.stories'

const props = {
  type: 'PDF',
  description: 'Dialog Description',
  loading: false,
  uploadFile: (url, cb) => {
    action('uploadingFile')(url)
    setTimeout(() => {
      action('uploadFile')(url)
      cb()
    }, 2000)
  }
}

storiesOf('materialCreator.fileUploadStep', module)
  .add('component', () => (
    <MuiThemeProvider>
      <div style={{width: 500, height: 300}}>
        <MaterialCreator {...props}>
          <PDFViewer {...pdfProps} />
        </MaterialCreator>
      </div>
    </MuiThemeProvider>
  ))
