import React from 'react'
import Dialog from 'material-ui/Dialog'
import TextField from 'material-ui/TextField'
import FlatButton from 'material-ui/FlatButton'
import RaisedButton from 'material-ui/RaisedButton'

import styles from './index.scss'

export default class FileUploadStep extends React.Component {
  state = {
    open: false,
    loading: false,
    url: ''
  }

  handleOpen = () => {
    this.setState({open: true})
  }

  handleClose = () => {
    this.setState({open: false})
  }

  onChangeUrl = (e) => {
    this.setState({url: e.target.value})
  }

  onSubmit = () => {
    this.setState({ loading: true })
    this.props.uploadFile(this.state.url, () => {
      this.setState({ loading: false })
      this.handleClose()
    })
  }

  render () {
    const {
      loading,
      url
    } = this.state

    const {
      children,
      type,
      description,
    } = this.props

    const actions = [
      <FlatButton
        label='Cancel'
        onClick={this.handleClose}
        disabled={loading}
        primary
      />,
      <FlatButton
        label='Submit'
        keyboardFocused={true}
        onClick={this.onSubmit}
        disabled={loading}
        primary
      />
    ]

    return (
      <div className={styles.root}>
        <h3 className={styles.title}>{type} Preview</h3>
        <RaisedButton
          className={styles.selectButton}
          label={`Select ${type}`}
          onClick={this.handleOpen}
          primary
        />
        <Dialog
          title='File Selector'
          actions={actions}
          modal={false}
          open={this.state.open}
          onRequestClose={this.handleClose}
        >
          <div>{description}</div>
          <TextField value={url} onChange={this.onChangeUrl} hintText='url' />
        </Dialog>
        <div className={styles.viewer}>
          {children}
        </div>
      </div>
    )
  }
}
