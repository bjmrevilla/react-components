import React from 'react'
import { storiesOf, action } from '@kadira/storybook'
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider'
import QuestionCreator from '../index'

const props = {
  question: {
    question: 'Question',
    answer: 0,
    questionType: 'identification'
  },
  onSave: action('onSave'),
  onCancel: action('onCancel')
}

storiesOf('materialCreator.questionCreator', module)
  .add('component', () => (
    <MuiThemeProvider>
      <div style={{width: 500, height: 300}}>
        <QuestionCreator {...props} />
      </div>
    </MuiThemeProvider>
  ))
  .add('edit', () => (
    <MuiThemeProvider>
      <div style={{width: 500, height: 300}}>
        <QuestionCreator {...props} edit />
      </div>
    </MuiThemeProvider>
  ))
