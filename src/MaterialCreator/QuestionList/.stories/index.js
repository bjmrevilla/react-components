import React from 'react'
import { storiesOf, action } from '@kadira/storybook'
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider'
import QuestionList from '../index'

storiesOf('materialCreator.questionList', module)
  .add('component', () => (
    <MuiThemeProvider>
      <div>
        <QuestionList
          type='Material'
          description='Lorem ipsum'
          onAddQuestion={action('onAddQuestion')}
        />
      </div>
    </MuiThemeProvider>
  ))
