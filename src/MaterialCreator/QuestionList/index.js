import React from 'react'
import TextField from 'material-ui/TextField'
import SelectField from 'material-ui/SelectField'
import MenuItem from 'material-ui/MenuItem'
import Dialog from 'material-ui/Dialog'
import RaisedButton from 'material-ui/RaisedButton'
import QuestionCreator from '../QuestionCreator'

import styles from './index.scss'

export default class QuestionList extends React.Component {
  constructor(props) {
    super(props)
    this.state = {genType: null, isCreatorOpen:false}
  }

  handleGenTypeChange = (e,index,value) => {
    this.setState({genType:value})
  }

  openCreator = () => {
    this.setState({isCreatorOpen: true})
  }

  closeCreator = () => {
    this.setState({isCreatorOpen: false})
  }

  onAddQuestion = (value) => {
    if (this.props.onAddQuestion) {
      this.props.onAddQuestion(value)
    }
    this.closeCreator()
  }

  render () {
    // TODO: Tabular
    const {type, description} = this.props
    return (
      <div className={styles.root}>
        <div className={styles.meta}>
          <h3>{type} Questions</h3>
          <p>{description}</p>
        </div>
        <SelectField
          style={{verticalAlign:'top'}}
          value={this.state.genType}
          onChange={this.handleGenTypeChange}
          floatingLabelText='Generation Type'
        >
          <MenuItem value='random' primaryText='random' />
          <MenuItem value='preset' primaryText='preset' />
        </SelectField>
        <TextField
          type='number'
          className={styles.numOfQuestions}
          floatingLabelText='no. of questions'
          onRequestClose={this.closeCreator}
          modal
        />
        <Dialog
          title='Question Creator'
          onRequestClose={this.closeCreator}
          open={this.state.isCreatorOpen}
        >
          <QuestionCreator
            question={{}}
            onSave={this.onAddQuestion}
            onCancel={this.closeCreator}
          />
        </Dialog>
        <RaisedButton
          label='Add'
          onClick={this.openCreator}
          className={styles.button}
          primary
        />
        <div>
          Tabular Stub
        </div>
      </div>
    )
  }
}
