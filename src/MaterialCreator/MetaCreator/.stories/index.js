import React from 'react'
import { storiesOf, action } from '@kadira/storybook'
import { compose, withState } from 'recompose'
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider'
import MetaCreator from '../index'

const Wrapped = compose(
  withState('meta', 'onChange', {
    title: 'title',
    author: 'author',
    description: 'description',
    publisher: 'publisher',
    publishedAt: 'Jan 16, 2009'
  })
)((props) => (
  <MuiThemeProvider>
    <MetaCreator {...props} />
  </MuiThemeProvider>
))

storiesOf('materialCreator.metaCreator', module)
  .add('component', () => (
    <Wrapped onUploadProfile={action('onUpload')} />
  ))
  .add('edit', () => (
    <Wrapped onUploadProfile={action('onUpload')} edit />
  ))
