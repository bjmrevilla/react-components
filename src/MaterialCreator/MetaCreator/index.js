import React from 'react'
import TextField from 'material-ui/TextField'
import RaisedButton from 'material-ui/RaisedButton'

import styles from './index.scss'

export default class QuestionCreator extends React.Component {
  static defaultProps = {
    edit: false,
    onChange: () => {}
  }

  change = (key) => (e) => {
    const {
      onChange,
      meta: {
        title,
        author,
        description,
        publisher,
        publishedAt
      },
      onUploadProfile
    } = this.props

    onChange({
      title,
      author,
      description,
      publisher,
      publishedAt,
      [key]: e.target.value
    })
  }

  render () {
    const { change } = this
    const {
      edit,
      meta: {
        title,
        image,
        author,
        description,
        publisher,
        publishedAt
      },
      onUploadProfile
    } = this.props

    return (
      <div className={styles.root}>
        <div className={styles.cover}>
          <div className={styles.imageHolder}>
            <img
              src={image ? image : ''}
              className={styles.image}
            />
          </div>
          <RaisedButton label='Upload Cover' onClick={onUploadProfile} fullWidth disabled={!edit}/>
        </div>
        <div>
          <TextField
            value={title}
            onChange={change('title')}
            floatingLabelText='Title'
            disabled={!edit}
            fullWidth
          />
          <TextField
            value={author}
            onChange={change('author')}
            floatingLabelText='Author'
            disabled={!edit}
            fullWidth
          />
          <TextField
            value={description}
            onChange={change('description')}
            floatingLabelText='Description'
            disabled={!edit}
            fullWidth
          />
          <TextField
            value={publisher}
            onChange={change('publisher')}
            floatingLabelText='Publisher'
            className={styles.publisher}
            disabled={!edit}
          />
          <TextField
            value={publishedAt}
            onChange={change('publishedAt')}
            floatingLabelText='Date published'
            className={styles.inline}
            disabled={!edit}
          />
        </div>
      </div>
    )
  }
}
