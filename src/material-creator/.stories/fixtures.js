const tags = [
  { key: 'Chip' },
  { key: 'Chap' },
  { key: 'Chop' }
];

const headers = [
  {
    label: 'All Lowercase',
    key: 'name',
    format: (x) => x.toLowerCase()
  },
  {
    label: 'All Uppercase',
    key: 'question',
    format: (x) => x.toUpperCase()
  },
  {
    label: 'header 3',
    key: 'h3',
    format: (x) => x
  }
];

const data = [
  {
    name: 'data 1-1',
    question: 'data 1-2',
    h3: 'data 1-3'
  },
  {
    name: 'data 2-1',
    question: 'data 2-2',
    h3: 'data 2-3'
  },
  {
    name: 'data 3-1',
    question: 'data 3-2',
    h3: 'data 3-3'
  }
];

export const material = {
  title: 'Material Design material',
  tags: tags,
  viewFilters: tags,
  exemptionFilters: tags,
  collaborators: tags,
  publishedVersions: data,
  drafts: data,
  questionList: {
    questions: data
  }
};