import React from 'react';
import {storiesOf, action} from '@kadira/storybook';
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';
import MaterialCreator from '../material-creator.component.jsx';
import {material} from './fixtures';

storiesOf('material-creator', module)
  .add('component', () => (
    <MuiThemeProvider>
      <MaterialCreator
        material={material}
        type='manual' />
    </MuiThemeProvider>
  ));

storiesOf('material-creator', module)
  .add('edit = true', () => (
    <MuiThemeProvider>
      <MaterialCreator
        material={material}
        type='guideline'
        edit={true} />
    </MuiThemeProvider>
  ));