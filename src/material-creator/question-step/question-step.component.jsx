'use strict';

import React from 'react';
import QuestionList from '../question-list/question-list.component.jsx';
import styles from './question-step.scss';

export default class QuestionStep extends React.Component {
  constructor(props) {
    super(props);
  }

  static propTypes = {
    questionList: React.PropTypes.object,
    edit: React.PropTypes.bool,
    onChange: React.PropTypes.func
  }

  static defaultProps = {
    edit: false
  }

  render = () => {
    return (
      <div className={styles.root}>
        <QuestionList
          questionList={this.props.questionList}
          edit={this.props.edit}
          onChange={this.props.onChange} />
      </div>
    );
  }
}