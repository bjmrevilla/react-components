'use strict';

import React from 'react';
import styles from './question-creator.scss';
import TextField from 'material-ui/TextField';
import SelectField from 'material-ui/SelectField';
import MenuItem from 'material-ui/MenuItem';
import FlatButton from 'material-ui/FlatButton';
import RaisedButton from 'material-ui/RaisedButton';

const Identification = ({
  question,
  answer,
  onChangeQuestion,
  onChangeAnswer}) => (
  <div>
    <TextField
      value={question}
      onChange={onChangeQuestion}
      floatingLabelText='Question' />
    <TextField
       value={answer}
       onChange={onChangeAnswer}
       floatingLabelText='Answer' />
  </div>
);

const MultipleChoice = ({
  question,
  answer,
  onChangeQuestion,
  onChangeAnswer,
  choices}) => (
  <div>
    <TextField
      value={question}
      onChange={onChangeQuestion}
      floatingLabelText='Question' />
    <SelectField
      value={answer}
      onChange={onChangeAnswer}
      hintText='Answer'>
      {choices.map(({key, label}) => (
        <MenuItem key={key} value={key} primaryText={label} />
      )) }
    </SelectField>
  </div>
);

export default class QuestionCreator extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      edit: false,
      questionType: null,
      question: null,
      answer: null
    };
  }

  static propTypes = {
    question: React.PropTypes.object,
    edit: React.PropTypes.bool,
    onSave: React.PropTypes.func
  }

  static defaultProps = {
    edit: false
  }

  componentDidMount = () => {
    const { edit, question, answer, questionType } = this.props;
    if (edit) {
      this.setState({
        question,
        answer,
        questionType
      });
    }
  }

  handleChange = (event, index, value) => {
    this.setState({ questionType: value, answer: null });
  }

  render = () => {
    const { edit, onCancel, onSave } = this.props;
    const { question, answer, questionType } = this.state;

    let questionForm = false;

    if (questionType == 'identification') {
      questionForm = (
        <Identification
          question={question}
          answer={answer}
          onChangeQuestion={e => this.setState({ question: e.target.value }) }
          onChangeAnswer={e => this.setState({ answer: e.target.value }) }
          />
      );
    } else if (questionType == 'multiple-choice') {
      questionForm = (
        <MultipleChoice
          question={question}
          answer={answer}
          choices={[{ key: 0, label: 'A' }]}
          onChangeQuestion={e => this.setState({ question: e.target.value }) }
          onChangeAnswer={(e, i, answer) => {
            this.setState({ answer })
          } }
          />
      );
    }

    return (
      <div className={styles.root}>
        <h3>{edit ? 'Edit' : 'Create'} Question</h3>
        <SelectField
          value={this.state.questionType}
          onChange={this.handleChange}
          hintText='Question Type'>
          <MenuItem value='identification' primaryText='Identification' />
          <MenuItem value='multiple-choice' primaryText='Multiple Choice' />
        </SelectField>
        {questionForm}
        <div className={styles.actions}>
          <FlatButton onClick={onCancel} label='Cancel' />
          <FlatButton
            label={edit ? 'Save' : 'Add'}
            onClick={e => onSave({ question, answer, questionType }) }
            disabled={!(questionType && answer !== null && question) }
            primary
            />
        </div>
      </div>
    );
  }
}