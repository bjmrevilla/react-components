'use strict';

import React from 'react';
import AboutStep from './about-step/about-step.component.jsx';
import FileUploadStep from './file-upload-step/file-upload-step.component.jsx';
import QuestionStep from './question-step/question-step.component.jsx';
import styles from './material-creator.scss';
import RaisedButton from 'material-ui/RaisedButton';
import FlatButton from 'material-ui/FlatButton';
import {
  Stepper,
  Step,
  StepLabel
} from 'material-ui/Stepper';
import Paper from 'material-ui/Paper';

export default class MaterialCreator extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      changes: {},
      stepIndex: 0
    };
  }

  static propTypes = {
    material: React.PropTypes.arrayOf(React.PropTypes.object),
    edit: React.PropTypes.bool,
    delete: React.PropTypes.func,
    type: React.PropTypes.string,
    save: React.PropTypes.func
  }

  static defaultProps = {
    edit: false
  }

  handleChange = (changes) => {
    // add field-value pairs to this.state.changes
    var newChanges = this.state.changes;
    var roleChanges = {};

    for (var field in changes) {
      var value = changes[field];

      // roleChanges[field] = value;
      newChanges[field] = value;
    }

    this.setState({ changes: newChanges });
  }

  handleCancel = () => {
    this.setState({
      changes: {}
    });
  }

  handleSave = () => {
    this.props.save(this.state.changes, (err) => {
      // should show loading and disable buttons
      // while saving
    });
  }

  handleNextStep = () => {
    if (this.state.stepIndex <= 2) {
      this.setState({
        stepIndex: this.state.stepIndex + 1
      });
    }
  }

  handleBackStep = () => {
    if (this.state.stepIndex >= 0) {
      this.setState({
        stepIndex: this.state.stepIndex - 1
      });
    }
  }

  getControls = (material) => {
    let mode = (this.props.edit)
      ? 'Edit:'
      : 'Create:';

    return (
      <div className={styles.controls}>
        <div className={styles.header}>
          <span className={styles.mode}>
            {mode}
          </span>
          <span className={styles.title}>
            {material.title}
          </span>
        </div>
        <div className={styles.saveControls}>
          <FlatButton
            label='Cancel'
            onTouchTap={this.handleCancel} />
          <RaisedButton
            label='Save'
            primary={true}
            onTouchTap={this.handleSave} />
        </div>
        <div className={styles.backNextControls}>
          <FlatButton
            label='Back'
            primary={true}
            onTouchTap={this.handleBackStep} />
          <FlatButton
            label='Next'
            primary={true}
            onTouchTap={this.handleNextStep} />
        </div>
      </div>
    );
  }

  getStepper = (material) => {
    return (
      <Stepper
        activeStep={this.state.stepIndex}
        className={styles.stepper}>
        <Step>
          <StepLabel>
            About {material.title}
          </StepLabel>
        </Step>
        <Step>
          <StepLabel>
            {material.title} Assessments
          </StepLabel>
        </Step>
        <Step>
          <StepLabel>
            Chapters/File Upload
          </StepLabel>
        </Step>
      </Stepper>
    );
  }

  getChild = (material) => {
    if (this.state.stepIndex === 0) {
      return <AboutStep
        material={material}
        edit={this.props.edit}
        type={this.props.type}
        changeVersion={this.props.changeVersion}
        onChange={this.handleChange}
        uploadProfilePic={this.props.uploadProfilePic} />;
    }
    if (this.state.stepIndex === 1) {
      return <QuestionStep
        questionList={this.props.material.questionList}
        edit={this.props.edit}
        onChange={this.handleChange} />;
    }
    if (this.state.stepIndex === 2) {
      return <FileUploadStep
        url={this.props.material.url}
        edit={this.props.edit}
        type={this.props.type}
        onChange={this.handleChange} />;
    }

    return '';
  }

  render = () => {
    return (
      <div className={styles.root}>
        {this.getControls(this.props.material) }
        {this.getStepper(this.props.material) }
        <Paper className={styles.child}>
          {this.getChild(this.props.material) }
        </Paper>
      </div>
    );
  }
}