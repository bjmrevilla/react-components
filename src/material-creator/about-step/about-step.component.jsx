'use strict';

import React from 'react';
import MetaCreator from '../meta-creator/meta-creator.component.jsx';
import styles from './about-step.scss';
import TagList from '../../ListView/TagList';
import TabularView
  from '../../list-view-page/tabular-view/tabular-view.component.jsx';

export default class AboutStep extends React.Component {
  constructor(props) {
    super(props);
  }

  static propTypes = {
    material: React.PropTypes.arrayOf(React.PropTypes.object),
    edit: React.PropTypes.bool,
    type: React.PropTypes.string,
    changeVersion: React.PropTypes.func,
    onChange: React.PropTypes.func
  }

  static defaultProps = {
    edit: false
  }

  getPublishedVersions = () => {
    if ((this.props.type !== 'manual')
      && (this.props.type !== 'guideline')) {
      return '';
    }

    let title = this.props.type;
    title = title.charAt(0).toUpperCase() + title.slice(1);
    let headers = [{
      label: 'Published Versions',
      key: 'name',
      format: (x) => x
    }];

    return (this.props.edit)
      ? (
        <div className={styles.publishedVersions}>
          <span className={styles.tableTitle}>
            {title} Published Versions
          </span>
          <TabularView
            headers={headers}
            data={this.props.material.publishedVersions} />
        </div>
      )
      : '';
  }

  getDrafts = () => {
    if ((this.props.type !== 'manual')
      && (this.props.type !== 'guideline')) {
      return '';
    }

    let title = this.props.type;
    title = title.charAt(0).toUpperCase() + title.slice(1);
    let headers = [{
      label: 'Drafts',
      key: 'name',
      format: (x) => x
    }];

    return (this.props.edit)
      ? (
        <div className={styles.drafts}>
          <span className={styles.tableTitle}>
            {title} Drafts
          </span>
          <TabularView
            headers={headers}
            data={this.props.material.drafts} />
        </div>
      )
      : '';
  }

  render = () => {
    return (
      <div className={styles.root}>
        <MetaCreator
          meta={this.props.material}
          edit={this.props.edit}
          onChange={this.props.onChange}
          uploadProfilePic={this.props.uploadProfilePic} />
        <div className={styles.tags}>
          <TagList
            title='Tags'
            tags={this.props.material.tags} />
          <TagList
            title='View Filters'
            tags={this.props.material.viewFilters} />
          <TagList
            title='Exemption Filters'
            tags={this.props.material.exemptionFilters} />
          <TagList
            title='Collaborators'
            tags={this.props.material.collaborators} />
        </div>
        {this.getPublishedVersions() }
        {this.getDrafts() }
      </div>
    );
  }
}