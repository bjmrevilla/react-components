'use strict';

import React from 'react';
import styles from './tabular-view.scss';
import {Table,
  TableBody,
  TableHeader,
  TableHeaderColumn,
  TableRow,
  TableRowColumn} from 'material-ui/Table';
import Popover from 'material-ui/Popover';
import Menu from 'material-ui/Menu';
import MenuItem from 'material-ui/MenuItem';
import Row from './row.component.jsx';

export default class TabularView extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      open: false,
      anchorEl: null
    }
  }

  static propTypes = {
    data: React.PropTypes.arrayOf(React.PropTypes.object),
    headers: React.PropTypes.arrayOf(React.PropTypes.shape({
      label: React.PropTypes.string,
      key: React.PropTypes.string,
      format: React.PropTypes.func
    })).isRequired,
    showSearch: React.PropTypes.bool,
    actions: React.PropTypes.arrayOf(React.PropTypes.shape({
      label: React.PropTypes.string,
      fn: React.PropTypes.func
    })),
    itemsToViewList: React.PropTypes.arrayOf(React.PropTypes.number)
  }

  static defaultProps = {
    showSearch: false,
    itemsToViewList: [5, 20, 50]
  }

  getHeadersRow = () => {
    return this.props.headers.map((header) => {
      return (
        <TableHeaderColumn>
          {header.label}
        </TableHeaderColumn>
      );
    });
  }

  getRows = () => {
    return this.props.data.map((rowObject) => {
      return <Row
        headers={this.props.headers}
        rowObject={rowObject}
        actions={this.props.actions} />;
    });
  }

  getActionsHeader = () => {
    return (this.props.actions)
      ? <TableHeaderColumn>Actions</TableHeaderColumn>
      : '';
  }

  render = () => {
    return (
      <div className={styles.root}>
        <Table>
          <TableHeader
            adjustForCheckbox={false}
            displaySelectAll={false}>
            <TableRow>
              {this.getHeadersRow()}
              {this.getActionsHeader()}
            </TableRow>
          </TableHeader>
          <TableBody>
            {this.getRows()}
          </TableBody>
        </Table>
      </div>
    );
  }
}