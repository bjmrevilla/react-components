'use strict';

import React from 'react';
import styles from './row.scss';
import {TableRow, TableRowColumn} from 'material-ui/Table';
import RaisedButton from 'material-ui/RaisedButton';
import Popover from 'material-ui/Popover';
import Menu from 'material-ui/Menu';
import MenuItem from 'material-ui/MenuItem';

export default class Row extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      isActionsOpen: false,
      anchorEl: null
    };
  }

  static propTypes = {
    headers: React.PropTypes.shape({
      label: React.PropTypes.string,
      key: React.PropTypes.string,
      format: React.PropTypes.func
    }),
    rowObject: React.PropTypes.object,
    actions: React.PropTypes.arrayOf(React.PropTypes.shape({
      label: React.PropTypes.string,
      fn: React.PropTypes.func
    }))
  }

  getColumns = () => {
    let row = this.props.headers.map((header) => {
      return (
        <TableRowColumn>
          {header.format(this.props.rowObject[header.key])}
        </TableRowColumn>
      );
    });

    if (this.props.actions) {
      row.push(this.getActionsButton());
    }
    
    return row;
  }

  getActionsButton = () => {
    return (
      <div className={styles.actionsContainer}>
        <RaisedButton
          className={styles.actionsButton}
          onTouchTap={this.handleTouchTap}
          label="Actions"
          primary />
        <Popover
          open={this.state.isActionsOpen}
          anchorEl={this.state.anchorEl}
          anchorOrigin={{ horizontal: 'left', vertical: 'bottom' }}
          targetOrigin={{ horizontal: 'left', vertical: 'top' }}
          onRequestClose={this.handleRequestClose}>
          <Menu>
            {this.getActions()}
          </Menu>
        </Popover>
      </div>
    );
  }

  getActions = () => {
    return this.props.actions.map((action) => {
      return (
        <MenuItem
          primaryText={action.label}
          onTouchTap={action.fn} />
      );
    });
  }

  handleTouchTap = (event) => {
    event.preventDefault();

    this.setState({
      isActionsOpen: true,
      anchorEl: event.currentTarget,
    });
  };

  handleRequestClose = () => {
    this.setState({
      isActionsOpen: false,
    });
  };

  render = () => {
    return (
      <TableRow className={styles.root}>
        {this.getColumns()}
      </TableRow>
    );
  }
}