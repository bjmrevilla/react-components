import React from 'react';
import styles from './full-page-view.scss';

export default class FullPageView extends React.Component {
  constructor(props) {
    super(props);
  }

  // all except itemsToViewList inherited from CarouselView
  static propTypes = {
    data: React.PropTypes.arrayOf(React.PropTypes.object),
    title: React.PropTypes.object, //ObjectKey
    subtitle: React.PropTypes.object,
    image: React.PropTypes.object,
    defaultImage: React.PropTypes.string,
    showSearch: React.PropTypes.bool,
    searchString: React.PropTypes.string,
    actions: React.PropTypes.arrayOf(React.PropTypes.shape({
      label: React.PropTypes.string,
      fn: React.PropTypes.func
    })),
    emphasizeIndices: React.PropTypes.arrayOf(React.PropTypes.number),
    itemsToViewList: React.PropTypes.arrayOf(React.PropTypes.number)
  }

  static defaultProps = {
    showSearch: false,
    itemsToViewList: [5, 20, 50]
  }

  render = () => {
    return (
      <div className={styles.root}>
      </div>
    );
  }
}