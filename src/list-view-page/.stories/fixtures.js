export const headers = [
  {
    label: 'All Lowercase',
    key: 'h1',
    format: (x) => x.toLowerCase()
  },
  {
    label: 'All Uppercase',
    key: 'h2',
    format: (x) => x.toUpperCase()
  },
  {
    label: 'header 3',
    key: 'h3',
    format: (x) => x
  }
];

export const data = [
  {
    h1: 'data 1-1',
    h2: 'data 1-2',
    h3: 'data 1-3'
  },
  {
    h1: 'data 2-1',
    h2: 'data 2-2',
    h3: 'data 2-3'
  },
  {
    h1: 'data 3-1',
    h2: 'data 3-2',
    h3: 'data 3-3'
  }
];

export const actions = [
  {
    label: 'action 1',
    fn: () => {}
  },
  {
    label: 'action 2',
    fn: () => {}
  }
];