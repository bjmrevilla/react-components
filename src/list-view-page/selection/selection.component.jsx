import React from 'react';
import styles from './selection.scss';

export default class Selection extends React.Component {
  constructor(props) {
    super(props);
  }

  static propTypes = {
    sourceItems: React.PropTypes.arrayOf(React.PropTypes.object),
    selectedItems: React.PropTypes.arrayOf(React.PropTypes.object),
    disableAddNew: React.PropTypes.bool,
    onSelectedItemsChanged: React.PropTypes.func,
    addFunction: React.PropTypes.func,
    key: React.PropTypes.string,
    search: React.PropTypes.func,
    title: React.PropTypes.string,
    addInstruction: React.PropTypes.string
  }

  static defaultProps = {
    disableAddNew: false
  }

  render = () => {
    return (
      <div className={styles.root}>
      </div>
    );
  }
}