'use strict';

export const utils = {
  search = (data, filter) => {
    let result = [];

    // worst case O(n^2) operation!
    for (let i in data) {
      let obj = data[i];

      for (let key in obj) {
        if (obj[key].indexOf(filter) >= 0) {
          result.push(obj);
          break;
        }
      }
    }

    return result;
  },
  sort = (data, key) => {
    data.sort((a, b) => {
      if (a[key] < b[key]) {
        return -1;
      }
      if (a[key] > b[key]) {
        return 1;
      }

      return 0;
    });
  },
  diff = (originalData, changedData, keysToExempt, keysToRemove) => {
    let changes = {};
    let removeKeys = (data, keysToRemove) => {
      for (let i in keysToRemove) {
        let keyToRemove = keysToRemove[i];
        // relies on side effects... not good functional programming?
        delete data[keyToRemove];
      }
    };
    // why is keysToRemove a 2d array?
    let isExempted = (key, keysToExempt) => {
      for (let i in keysToExempt) {
        if (key === keysToExempt[i]) return true;
      }

      return false;
    }; // eesh... compounds complexity with length of keysToExempt

    // REMOVE
    removeKeys(originalData, keysToRemove);
    removeKeys(changedData, keysToRemove);

    // worst case O(n) operation?
    for (let key in originalData) {
      // EXEMPT
      if (!isExempted(key, keysToExempt)
          && (originalData[key] !== changedData[key])) {
        changes[key] = changedData[key];
      }
    }

    return changes;
  }
}