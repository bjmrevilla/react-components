import React from 'react';
import styles from './tags-list.scss';

export default class TagsList extends React.Component {
  constructor(props) {
    super(props);
  }

  static propTypes = {
    tags: React.PropTypes.arrayOf(React.PropTypes.object),
    title: React.PropTypes.string,
    description: React.PropTypes.string,
    tip: React.PropTypes.string,
    disabled: React.PropTypes.bool,
    showTitle: React.PropTypes.bool,
    showDescription: React.PropTypes.bool,
    showTip: React.PropTypes.bool,
    disableAddNew: React.PropTypes.bool,
    onTagsChanged: React.PropTypes.func,
    sourceTags: React.PropTypes.arrayOf(React.PropTypes.object),
    disableRemove: React.PropTypes.bool,
    hideFloatingAddButton: React.PropTypes.bool,
    readOnly: React.PropTypes.bool,
    key: React.PropTypes.string,
    hideAddButton: React.PropTypes.bool 
  }

  static defaultProps = {
    disabled: false,
    showTitle: true,
    showDescription: true,
    showTip: true,
    disableAddNew: true,
    disableRemove: false,
    hideFloatingAddButton: false,
    readOnly: false,
    hideAddButton: false
  }

  render = () => {
    return (
      <div className={styles.root}>
      </div>
    );
  }
}