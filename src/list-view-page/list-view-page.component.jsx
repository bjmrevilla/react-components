import React from 'react';
import styles from './list-view-page.scss';

export default class ListViewPage extends React.Component {
  constructor(props) {
    super(props);
  }

  static propTypes = {
    data: React.PropTypes.arrayOf(React.PropTypes.object),
    title: React.PropTypes.string,
    viewModes: React.PropTypes.arrayOf(React.PropTypes.oneOf([
      'tabular',
      'carousel',
      'fullpage'
    ])),
    onAddNewTouchTap: React.PropTypes.func,
    enableSearch: React.PropTypes.bool,
    groupByKey: React.PropTypes.arrayOf(React.PropTypes.shape({
      path: React.PropTypes.arrayOf(React.PropTypes.string),
      title: React.PropTypes.string,
      showTitle: React.PropTypes.bool
    })),
    showFloatingAddNew: React.PropTypes.bool
  }

  static defaultProps = {
    viewModes: ['tabular'],
    enableSearch: true,
    showFloatingAddNew: false
  }

  render = () => {
    return (
      <div className={styles.root}>
      </div>
    );
  }
}