import React from 'react';
import styles from './thumbnail.scss';

export default class Thumbnail extends React.Component {
  constructor(props) {
    super(props);
  }

  static propTypes = {
    data: React.PropTypes.object,
    title: React.PropTypes.object,
    subtitle: React.PropTypes.object,
    image: React.PropTypes.object,
    defaultImage: React.PropTypes.string,
    hideImage: React.PropTypes.bool,
    emphasize: React.PropTypes.bool,
    imageOnly: React.PropTypes.bool,
    actions: React.PropTypes.arrayOf(React.PropTypes.shape({
      label: React.PropTypes.string,
      fn: React.PropTypes.func
    }))
  }

  static defaultProps = {
    hideImage: false,
    emphasize: false,
    imageOnly: false
  }

  render = () => {
    return (
      <div className={styles.root}>
      </div>
    );
  }
}