'use strict';

import React from 'react';
import {Card, CardActions, CardHeader, CardMedia, CardTitle, CardText} from 'material-ui/Card'
import NavigationArrowDropDown from 'material-ui/svg-icons/navigation/arrow-drop-down';
import NavigationArrowDropUp from 'material-ui/svg-icons/navigation/arrow-drop-up';
import RaisedButton from 'material-ui/RaisedButton';
import FlatButton from 'material-ui/FlatButton';
import Subheader from 'material-ui/Subheader';
import IconButton from 'material-ui/IconButton';

import styles from './answer.component.scss' 

export default class Answer extends React.Component {
  constructor() {
    super();

    this.state = {

    }
  }

  render() {
    const {
      answer,
      image
    } = this.props;

    return(
      <div>
        <Card>
          <CardHeader
            title={answer.creator.username}
            subtitle={answer.creator.email}
            avatar={image}
          >  
          <div className={styles.createdAt}>{answer.createdAt}</div>
          </CardHeader>
          <CardText>
            {answer.body}
          </CardText>
          <CardActions>
            <RaisedButton 
              label='Show Comments'
              className={styles.raisedButton}
              primary
            />
            <div className={styles.votes}>
              <FlatButton 
                label='Upvote'
                className={styles.voteButtons}
                primary
              />
              <Subheader>{answer.votes}</Subheader>
              
              <FlatButton 
                label='Downvote'
                primary
              />
            </div>
            
          </CardActions>
        </Card>
      </div>
    );
  }
}