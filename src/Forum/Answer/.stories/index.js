'use strict';

import React from 'react';
import { storiesOf, action } from '@kadira/storybook';
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';
import Answer from '../answer.component.js';
import { answer } from './answer.component.data.js';

storiesOf('forum.answerComponent', module)
  .add('component', () => (
    <MuiThemeProvider>
      <Answer
        image='http://i.imgur.com/6jr3M0j.png'
        answer={answer}
      />
    </MuiThemeProvider>
  ))
  
