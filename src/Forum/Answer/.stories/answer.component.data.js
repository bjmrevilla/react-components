
export const answer = {
  creator: {
    username: 'bjmrevilla',
    email: 'bjmrevilla@gmail.com'
  },
  body: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam at molestie est. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Mauris tempor tortor consequat scelerisque posuere. Curabitur ut felis sit amet sapien commodo dictum. In tempus, ipsum euismod vulputate auctor, velit diam vulputate ligula, in sodales libero nulla eget nisl. Sed tristique, urna ut faucibus semper, tellus enim mollis ante, in iaculis urna turpis at odio. Vivamus gravida sem elit, sed finibus ligula tempus non. Duis est tortor, commodo et justo ut, ullamcorper dictum sapien.',
  createdAt: '12:12pm October 6, 2016',
  votes: 89
}