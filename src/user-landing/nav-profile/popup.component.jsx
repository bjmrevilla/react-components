'use strict';

import React from 'react';
import Avatar from 'material-ui/Avatar';
import Account from 'material-ui/svg-icons/action/account-circle';
import RaisedButton from 'material-ui/RaisedButton';

import styles from './popup.scss';

export default class Popup extends React.Component {
  render = () => {
    const { account: { username, email } } = this.props;
    const {
      onProfileTouchTap,
      onSettingsTouchTap,
      onLogoutTouchTap
    } = this.props;

    return (
      <div className={styles.root}>
        <img
          className={styles.avatar}
          src='http://www.material-ui.com/images/uxceo-128.jpg'
        />
        <h2>{username}</h2>
        <h3>{email}</h3>
        <RaisedButton
          className={styles.button}
          label='Profile'
          onClick={onProfileTouchTap}
          fullWidth
          primary
        />
        <RaisedButton
          className={styles.button}
          label='Settings'
          onClick={onSettingsTouchTap}
          fullWidth
          primary
        />
        <RaisedButton
          className={styles.button}
          label='Logout'
          onClick={onLogoutTouchTap}
          fullWidth
          primary
        />
      </div>
    );
  }
}
