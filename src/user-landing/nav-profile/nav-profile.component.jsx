'use strict';

import React from 'react';
import Popup from './popup.component.jsx';
import styles from './nav-profile.scss';
import IconButton from 'material-ui/IconButton';
import Popover from 'material-ui/Popover';

export default class NavProfile extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      isPopoverOpen: false,
      anchorEl: null
    };
  }

  static propTypes = {
    account: React.PropTypes.object.isRequired,
    onProfileTouchTap: React.PropTypes.func,
    showProfile: React.PropTypes.bool,
    onSettingsTouchTap: React.PropTypes.func,
    showSettings: React.PropTypes.bool,
    onLogoutTouchTap: React.PropTypes.func,
    showLogout: React.PropTypes.bool
  }

  static defaultProps = {
    showProfile: true,
    showSettings: true,
    showLogout: true
  }

  handleTouchTap = (event) => {
    event.preventDefault();
    
    this.setState({
      isPopoverOpen: true,
      anchorEl: event.currentTarget
    });
  }

  handleRequestClose = () => {
    this.setState({
      isPopoverOpen: false
    });
  }

  render = () => {
    return (
      <div className={styles.root}>
        <IconButton
          iconClassName={styles.icon}
          onTouchTap={this.handleTouchTap}
          tooltip='Profile'>
					<svg xmlns="http://www.w3.org/2000/svg" width="18" height="18" viewBox="0 0 18 18"><path d="M9 8c1.66 0 2.99-1.34 2.99-3S10.66 2 9 2C7.34 2 6 3.34 6 5s1.34 3 3 3zm0 2c-2.33 0-7 1.17-7 3.5V16h14v-2.5c0-2.33-4.67-3.5-7-3.5z"/></svg>
        </IconButton>
        <Popover
          open={this.state.isPopoverOpen}
          anchorEl={this.state.anchorEl}
          anchorOrigin={{horizontal: 'right', vertical: 'bottom'}}
          targetOrigin={{horizontal: 'right', vertical: 'top'}}
          onRequestClose={this.handleRequestClose}>
          <Popup
            account={this.props.account}
            onProfileTouchTap={this.props.onProfileTouchTap}
            onSettingsTouchTap={this.props.onSettingsTouchTap}
            onLogoutTouchTap={this.props.onLogoutTouchTap}
            showProfile={this.props.showProfile}
            showSettings={this.props.showSettings}
            showLogout={this.props.showLogout} />
        </Popover>
      </div>
    );
  }
}