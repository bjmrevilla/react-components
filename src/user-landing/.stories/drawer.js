import React from 'react'
import { storiesOf, action } from '@kadira/storybook'
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider'
import Drawer from '../drawer/drawer.component.jsx'
import {testAccount,
  testOnDrawerToggle,
  menus,
  themes} from './fixtures'

storiesOf('user-landing', module)
  .add('drawer', () => (
    <MuiThemeProvider>
      <Drawer open={true} menus={menus} themes={themes} />
    </MuiThemeProvider>
  ));