import React from 'react'
import { storiesOf, action } from '@kadira/storybook'
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider'
import NavMessages from '../nav-messages/nav-messages.component.jsx'
import {messages} from './fixtures';

storiesOf('user-landing', module)
  .add('nav-messages', () => (
    <MuiThemeProvider>
      <NavMessages messages={messages} />
    </MuiThemeProvider>
  ));