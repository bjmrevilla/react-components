import React from 'react'
import { storiesOf, action } from '@kadira/storybook'
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider'
import AppBar from '../app-bar/app-bar.component.jsx'
import {testAccount,
  testOnDrawerToggle} from './fixtures'

storiesOf('user-landing', module)
  .add('app-bar', () => (
    <MuiThemeProvider>
      <AppBar
        title='Kalahi'
        account={testAccount}
        onDrawerToggle={testOnDrawerToggle} />
    </MuiThemeProvider>
  ));