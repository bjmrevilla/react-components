import React from 'react'
import { storiesOf, action } from '@kadira/storybook'
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider'
import NavNotifs from '../nav-notifs/nav-notifs.component.jsx'
import {notifications} from './fixtures';

storiesOf('user-landing', module)
  .add('nav-notifs', () => (
    <MuiThemeProvider>
      <NavNotifs notifications={notifications} />
    </MuiThemeProvider>
  ));