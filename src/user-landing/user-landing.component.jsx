'use strict';

import React from 'react';
import AppBar from './app-bar/app-bar.component.jsx';
import Drawer from './drawer/drawer.component.jsx';
import styles from './user-landing.scss';

export default class UserLanding extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      isDrawerOpen: false
    }
  }

  static propTypes = {
    children: React.PropTypes.any.isRequired,
    account: React.PropTypes.object.isRequired
  }

  onDrawerToggle = (open) => {
    this.setState({
      isDrawerOpen: open
    });
  }

  onRequestChange = () => {
    this.setState({
      isDrawerOpen: false
    });
  }

  render = () => {
    return (
      <div className={styles.root}>
        <AppBar
          onDrawerToggle={this.onDrawerToggle}
          title={this.props.title}
          account={this.props.account}
          showNavProfile={this.props.showNavProfile}
          showNavMessages={this.props.showNavMessages}
          showNavNotifs={this.props.showNavNotifs}
          isDrawerOpen={this.state.isDrawerOpen}
          onProfileTouchTap={this.props.onProfileTouchTap}
          onSettingsTouchTap={this.props.onSettingsTouchTap}
          onLogoutTouchTap={this.props.onLogoutTouchTap}
          showProfile={this.props.showProfile}
          showSettings={this.props.showSettings}
          showLogout={this.props.showLogout}
          onMessageTouchTap={this.props.onMessageTouchTap}
          onSeeAllTouchTap={this.props.onSeeAllTouchTap}
          showSeeAll={this.props.showSeeAll}
          onNotificationTouchTap={this.props.onNotificationTouchTap} />
        <Drawer
          open={this.state.isDrawerOpen}
          themes={this.props.themes}
          menus={this.props.menus}
          onThemeChange={this.props.onThemeChange}
          onRequestChange={this.onRequestChange} />
        <div className={styles.child}>
          {this.props.children}
        </div>
      </div>
    );
  }
}