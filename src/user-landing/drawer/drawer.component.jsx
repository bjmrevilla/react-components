'use strict';

import React from 'react';
import styles from './drawer.scss';
import MaterialUIDrawer from 'material-ui/Drawer';
import {List, ListItem} from 'material-ui/List';
import SelectField from 'material-ui/SelectField';
import MenuItem from 'material-ui/MenuItem';

export default class Drawer extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      themeIndex: 0
    };
  }

  static propTypes = {
    open: React.PropTypes.bool,
    themes: React.PropTypes.arrayOf(React.PropTypes.string),
    menus: React.PropTypes.arrayOf(React.PropTypes.shape({
      key: React.PropTypes.any.isRequired,
      icon: React.PropTypes.node,
      label: React.PropTypes.string.isRequired,
      link: React.PropTypes.string.isRequired,
      onTouchTap: React.PropTypes.func,
      childMenus: React.PropTypes.array //array of itself
    })),
    onThemeChange: React.PropTypes.func,
    onRequestChange: React.PropTypes.func //workaround to close
  }

  static defaultProps = {
    open: false
  }

  handleThemeChange = (index) => {
    this.props.onThemeChange(this.props.themes[index]);
    this.setState({ themeIndex: index });
  }

  // takes in menus param so I can use recursively for nested menus
  getMenus = (menus) => {
    return menus.map((menu) => {
      const doNestedItemsExist = (menu.childMenus) ? true : false;
      const doesLinkExist = (menu.link) ? true : false;
      const nestedItems = (doNestedItemsExist)
        ? this.getMenus(menu.childMenus)
        : '';

      return (doNestedItemsExist)
        ? <ListItem
          key={menu.key}
          primaryText={menu.label}
          leftIcon={menu.icon}
          primaryTogglesNestedList={doNestedItemsExist}
          nestedItems={nestedItems} />
        : (doesLinkExist)
          ? <ListItem
            key={menu.key}
            primaryText={menu.label}
            leftIcon={menu.icon}
            href={menu.link} />
          : <ListItem
            key={menu.key}
            primaryText={menu.label}
            leftIcon={menu.icon}
            onClick={menu.onTouchTap} />;
    });
  }

  getThemes = () => {
    return (this.props.themes)
      ? this.props.themes.map((theme, index) => {
        return <MenuItem
          value={index}
          primaryText={theme} />
      })
      : '';
  }

  render = () => {
    return (
      <MaterialUIDrawer
        className={styles.root}
        open={this.props.open}
        docked={false}
        onRequestChange={this.props.onRequestChange}>
        <List>
          {this.getMenus(this.props.menus) }
        </List>
        <div className={styles.themePicker}>
          <SelectField
            value={this.state.themeIndex}
            floatingLabelText='Theme'
            onChange={this.handleThemeChange}>
            {this.getThemes() }
          </SelectField>
        </div>
      </MaterialUIDrawer>
    );
  }
}