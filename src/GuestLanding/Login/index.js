'use strict';

import React, { PropTypes } from 'react';
import TextField from 'material-ui/TextField';
import RaisedButton from 'material-ui/RaisedButton';
import Paper from 'material-ui/Paper';

import styles from './index.scss'

export default class Login extends React.Component {
  constructor () {
    super()
    this.state = {
      open: false,
      username: '',
      password: '',
      snackBarText: '',
      usernameErrorText: '',
      passwordErrorText: ''
    }
  }

  static defaultProps = {
    usernameHint: 'Username',
    passwordHint: 'Password'
  }

  onUsernameChange = e => this.setState({ username: e.target.value })
  onPassChange = e => this.setState({ password: e.target.value })

  render = () => {
    return (
      <Paper className={styles.root}>
        <h1>Kalahi CIDDS</h1>
        <TextField
          hintText={this.usernameHint}
          onChange={this.onUsernameChange}
          value={this.props.username}
          errorText={this.props.usernameErrorText}
          floatingLabelText="Username"
        />
        <br />
        <TextField
          hintText={this.passwordHint}
          onChange={this.onPassChange}
          value={this.props.password}
          errorText={this.props.passwordErrorText}
          floatingLabelText="Password"
          type="password"
        />
        <br />
        <br />
        <RaisedButton
          label='Login'
          onClick={this.props.onClick}
          fullWidth
          primary
        />
      </Paper>
    )
  }
}