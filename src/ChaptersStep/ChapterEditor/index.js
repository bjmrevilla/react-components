import React, { PropTypes }  from 'react'
import scss from './index.scss'
import TinyMCE from 'react-tinymce-input'
import TextField from 'material-ui/TextField'
import RaisedButton from 'material-ui/RaisedButton'

export default class ChapterEditor extends React.Component {
  constructor () {
    super()
  }

  static propTypes = {
    chapter: PropTypes.shape({
      title: PropTypes.string,
      number: PropTypes.number,
      content: PropTypes.string
    }),
    editable: PropTypes.bool,
    onChange: PropTypes.func,
    onChangeTitle: PropTypes.func,
    onChangeNumber: PropTypes.func,
    onQuestions: PropTypes.func,
    onDelete: PropTypes.func
  }

  static defaultProps = {
    editable: true
  }

  onChangeTitle (e) { 
    e.persist()
    const { value } = e.target
    this.props.onChangeTitle(value)
  }

  onChangeNumber (e) {
    e.persist()
    const { value } = e.target
    this.props.onChangeNumber(value)
  }

  onChangeContent (content) {
    this.props.onChange(content)
  }

  render () {
    const { props, onChangeTitle, onChangeContent, onChangeNumber } = this

    const { chapter, editable, onDelete, onQuestions } = props
    const { number, title, content } = chapter

    return (
      <div className={scss.root} style={styles.container}>
        <div style={styles.inputRow}>
          <TextField
            style={styles.editorNumber}
            type='text'
            value={number}
            placeholder='#'
            onChange={onChangeNumber.bind(this)}
            disabled={!editable}
          />
          <TextField
            style={styles.editorTitle}
            type='text'
            value={title}
            placeholder='Enter chapter title here'
            onChange={onChangeTitle.bind(this)}
            disabled={!editable}
          />
          <RaisedButton label='Questions' primary onClick={onQuestions} />
        </div>
        <div style={styles.editorChapter}>
          <TinyMCE
            ref='tinyMCE'
            tinymceConfig={{
              plugins: 'autolink link image table lists paste preview',
              height: '400',
              automatic_uploads: true,
              paste_data_images: true,
              images_upload_url: '/cdn/upload/',
              images_upload_base_path: '/cdn/',
              images_upload_credentials: true,
              content_style: 'img { height: auto; max-width: 100%; }',
              readonly: !editable
            }}
            value={content}
            onChange={onChangeContent.bind(this)}
          />
        </div>
        <div>
          <h1>Danger Zone!</h1>
          <RaisedButton label='Delete' secondary onClick={onDelete} />
        </div>
      </div>
    )
  }
}

const styles = {
  container: {
    backgroundColor: 'white',
    borderColor: '#e8e8e8',
    borderStyle: 'solid',
    borderRadius: 6,
    borderWidth: 1,
    boxSizing: 'border-box',
    padding: '1rem 2rem 1rem'
  },
  inputRow: {
    width: '100%'
  },
  editorNumber: {
    border: 0,
    boxSizing: 'border-box',
    fontSize: '2rem',
    fontWeight: 'bold',
    outline: 'none',
    marginRight: '1rem',
    width: '3rem'
  },
  editorTitle: {
    border: 0,
    boxSizing: 'border-box',
    fontSize: '2rem',
    fontWeight: 'bold',
    marginRight: '1rem',
    outline: 'none'
  },
  editorChapter: {
    paddingTop: '1rem'
  }
}
