import React from 'react'
import { compose, withHandlers, withState } from 'recompose'
import { storiesOf, action } from '@kadira/storybook'
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider'
import ChapterEditor from '../index'

const chapter = {
  title: 'Chapter Title',
  number: 1,
  content: ''
}
const edit = true

const Wrapped = compose(
  withState('title', 'onChangeTitle', 'Sample Title'),
  withState('number', 'onChangeNumber', 1),
  withState('content', 'onChange', 'Sample Content'),
)(({ title, number, content, onChange, onChangeTitle, onChangeNumber, ...props }) => (
  <MuiThemeProvider>
    <ChapterEditor
      chapter={{ title, number, content }}
      onChange={onChange}
      onChangeNumber={onChangeNumber}
      onChangeTitle={onChangeTitle}
      onQuestions={action('onQuestions')}
      onDelete={action('onDelete')}
      {...props}
    />
  </MuiThemeProvider>
))

storiesOf('chaptersStep.chapterEditor', module)
  .add('component', () => <Wrapped />)
  .add('not editable', () => <Wrapped editable={false} />)
