import React from 'react'
import Avatar from 'material-ui/Avatar'
import Account from 'material-ui/svg-icons/action/account-circle'
import RaisedButton from 'material-ui/RaisedButton'

import styles from './index.scss'

export default class NavProfile extends React.Component {
  render () {
    const { account: { username, email } } = this.props
    const { onProfile, onSettings, onLogout } = this.props

    return (
      <div className={styles.root}>
        <img
          className={styles.avatar}
          src='http://www.material-ui.com/images/uxceo-128.jpg'
        />
        <h2>{username}</h2>
        <h3>{email}</h3>
        <RaisedButton
          className={styles.button}
          label='Profile'
          onClick={onProfile}
          fullWidth
          primary
        />
        <RaisedButton
          className={styles.button}
          label='Settings'
          onClick={onSettings}
          fullWidth
          primary
        />
        <RaisedButton
          className={styles.button}
          label='Logout'
          onClick={onLogout}
          fullWidth
          primary
        />
      </div>
    )
  }
}
