import React from 'react'
import { storiesOf, action } from '@kadira/storybook'
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider'
import NavProfile from '../index'

storiesOf('NavProfile', module)
  .add('component', () => (
    <MuiThemeProvider>
      <div style={{width: 250}}>
        <NavProfile
          account={{
            username: 'username',
            email: 'email'
          }}
          onProfile={action('profile')}
          onSettings={action('settings')}
          onLogout={action('logout')}
        />
      </div>
    </MuiThemeProvider>
  ))
