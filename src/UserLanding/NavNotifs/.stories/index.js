import React from 'react'
import { storiesOf, action } from '@kadira/storybook'
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider'
import NavNotifs from '../index.js'

const notifications = [
  {
    id: 1,
    type: 'Username',
    body: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque dignissim velit non metus tristique',
  },
  {
    id: 2,
    type: 'Username',
    body: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque dignissim velit non metus tristique',
  },
  {
    id: 3,
    type: 'Username',
    body: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque dignissim velit non metus tristique',
  }
]

storiesOf('NavNotifs', module)
  .add('component', () => (
    <MuiThemeProvider>
      <div style={{ width: 250 }}>
        <NavNotifs
          notifications={notifications}
          onNotification={action('onNotification')}
          onSeeAll={action('onSeeAll')}
        />
      </div>
    </MuiThemeProvider>
  ))
  .add('!showSeeAll', () => (
    <MuiThemeProvider>
      <div style={{ width: 250 }}>
        <NavNotifs
          notifications={notifications}
          onNotification={action('onNotification')}
          onSeeAll={action('onSeeAll')}
          showSeeAll={false}
        />
      </div>
    </MuiThemeProvider>
  ))
