import React from 'react'
import { storiesOf, action } from '@kadira/storybook'
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider'
import Appbar from '../index'

storiesOf('Appbar', module)
  .add('component', () => (
    <MuiThemeProvider>
      <Appbar title='Kalahi' />
    </MuiThemeProvider>
  ));
