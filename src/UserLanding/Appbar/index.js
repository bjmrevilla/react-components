import React from 'react'
import Bar from 'material-ui/AppBar'

export default ({ title }) => (
  <Bar
    title={title}
    iconClassNameRight='muidocs-icon-navigation-expand-more'
  />
)
