import React from 'react'
import { storiesOf, action } from '@kadira/storybook'
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider'
import NavMessages from '../index.js'

const messages = [
  {
    id: 1,
    title: 'Username',
    subtitle: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque dignissim velit non metus tristique',
    avatar: 'http://www.material-ui.com/images/ok-128.jpg'
  },
  {
    id: 2,
    title: 'Username 2',
    subtitle: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque dignissim velit non metus tristique',
    avatar: 'http://www.material-ui.com/images/ok-128.jpg'
  },
  {
    id: 3,
    title: 'Username 3',
    subtitle: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque dignissim velit non metus tristique',
    avatar: 'http://www.material-ui.com/images/ok-128.jpg'
  }
]

storiesOf('NavMessages', module)
  .add('component', () => (
    <MuiThemeProvider>
      <div style={{ width: 250 }}>
        <NavMessages
          messages={messages}
          onMessage={action('onMessage')}
          onSeeAll={action('onSeeAll')}
        />
      </div>
    </MuiThemeProvider>
  ))
  .add('!showSeeAll', () => (
    <MuiThemeProvider>
      <div style={{ width: 250 }}>
        <NavMessages
          messages={messages}
          onMessage={action('onMessage')}
          onSeeAll={action('onSeeAll')}
          showSeeAll={false}
        />
      </div>
    </MuiThemeProvider>
  ))
